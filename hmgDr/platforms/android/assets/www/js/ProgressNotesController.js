var ProgressNotesController = ProgressNotesController || {};
ProgressNotesController = (function () {
var DisplayProgressNotesPage="#progress-notes-page";
var newProgressNotePage="#new-progress-notes-page";
var progressNoteList=[];
var currentNote;
var isEditNote=false;
var currentNoteAll;
var isNoteVerified=false;
var isCancelledNote=false;
var isInpatientOrder=false;
  function init() {
     $(DisplayProgressNotesPage).on("pagebeforeshow", function () {
	 if(App.currentPatient.isDischarged){
	 $("#addNewProgNoteBtn").hide();
	 }else{
	  $("#addNewProgNoteBtn").show();
	 }
	 if(isInpatientOrder){
	 $("#prog-Header").empty().append(Loc.progNotePage.orderSheet);
	 getInpatientOrders();
	 }else{
	  $("#prog-Header").empty().append(Loc.progNotePage.progNote);
	 getProgressNote();
	 }
	});	
	$(DisplayProgressNotesPage).on("pagehide", function () {
	});
	$(newProgressNotePage).on("pageshow", function () {
       if(isEditNote==true){
	   $("#verifyProgNote").attr('disabled',false);
	   }else{
	     $("#verifyProgNote").attr('disabled',true);
	   }
	});
	$(newProgressNotePage).on("pagebeforeshow", function () {
	 if(isInpatientOrder){
	$("#newProgressHeader").empty().append(Loc.progNotePage.orderNew);
	}else{
	$("#newProgressHeader").empty().append(Loc.progNotePage.newProg);
	}
       $("#progNotes").val('');
	});
	$(newProgressNotePage).on("pagehide", function () {
       isEditNote=false;
	});
    $(document).on('pagebeforeshow', '#progress-notes-page', function(){       
});	
  } // init()
	function getProgressNote(){
	isInpatientOrder=false;
	 var body = {
         VisitType: 5,
		 AdmissionNo:parseInt(App.currentPatient.AdmissionNo),
		ProjectID:App.currentPatient.ProjectID
		//AdmissionNo: 2014000010,
		//ProjectID:12
       };
       var request = new Req.RequestModel("GtProgressNotes", body, retrieveProgressNotesSuccess, retrieveProgressNotesError);
       Req.sendRequest(request);
	}
function getInpatientOrders(){
	isInpatientOrder=true;
 var body = {
         VisitType: 3,
		 AdmissionNo:parseInt(App.currentPatient.AdmissionNo),
		 ProjectID:App.currentPatient.ProjectID
		// AdmissionNo: 2014000010,
		// ProjectID:12
       };
       var request = new Req.RequestModel("GtProgressNotes", body, retrieveProgressNotesSuccess, retrieveProgressNotesError);
       Req.sendRequest(request);
}
  function retrieveProgressNotesSuccess(result){
  var list=[];
  list=result.List_GetPregressNoteForInPatient;
    progressNoteList=list;
		 DisplayProgressNotesPageResults(); 
  $.mobile.changePage("#progress-notes-page"); 
  }
  function retrieveProgressNotesError(result){
  progressNoteList=[];
		 DisplayProgressNotesPageResults(); 
  $.mobile.changePage("#progress-notes-page"); 
  }
  function DisplayProgressNotesPageResults(){
    $('#progress-notes-page').trigger('create');
	 $("#collapsiblesetForNotes").empty().collapsibleset('refresh');
	if(progressNoteList.length>0){
  var progressNoteListLength = progressNoteList.length;
  var ul=$("#collapsiblesetForNotes");
  var innerHtml=""
  var editButtonHTML="";
  		 var clinicName="";
		 for(var i = 0; i < progressNoteListLength; i++)
		 {
		 var createOn=progressNoteList[i].CreatedOn;
		 if(progressNoteList[i].DoctorClinicName){
		 clinicName=progressNoteList[i].DoctorClinicName;
		 }else{
		 clinicName="";
		 }
		 createOn=App.ConvertDate(createOn);
		 if((progressNoteList[i].Status==1) && (progressNoteList[i].CreatedBy == App.user.DoctorID) && (App.currentPatient.isDischarged==false)){
		 editButtonHTML='<td style="text-align:center;"><input id="editProgNote" type="image" src="img/addprogressnote/edit.png" height="30" width="30" onclick="ProgressNotesController.editNotePage('+i+');"/><br><span  style="font-size: 10px; text-align: right;">' + createOn + '</span></td>';
		}else if(progressNoteList[i].Status==4) {
		 editButtonHTML='<td style="text-align:center;"><div  style="font-size: 10px; text-align: right;color:red;">' + Loc.progNotePage.cancelled + '</div></td>';
		 }else{
		 editButtonHTML='<td style="text-align:center;"><div  style="font-size: 10px; text-align: right;">' + createOn + '</div></td>';
		 }
	 // innerHtml = '<h6 class="" ><table width="100%"><tr><td text-align="left" style="font-size: 12px;">Enas Ziad Azzoqqqa</br><span style=" color:#ed1b2c; font-size: 10px; ">' + progressNoteList[i].VisitType + '</span></td><td  style="font-size: 14px; text-align: right;">' + dateFormat(createOn, "dd/mm/yyyy")+ '</td><tr></table></h6>' + '<ul data-role="listview"">'
      // innerHtml += '<li data-icon="false" style="list-style-type: none;"><table align="center" width="100%" id="progressNoteTableTheme" ><tr><td style="border-left: 4px double #EE3531; padding-left: 5px;">' + progressNoteList[i].Notes+'</td></tr></table></li>';
 	 innerHtml = '<h6 class="" ><table width="100%"><tr><td text-align="left" style="width:70%; font-size: 12px;">'+progressNoteList[i].DoctorName+'</br><span style=" color:#ed1b2c; font-size: 10px; ">' + clinicName + '</span></td>'+editButtonHTML+'<tr></table></h6>'+  '<ul data-role="listview"">' 

  innerHtml += '<li data-icon="false" class="customListNote" style="list-style-type: none; border: 1px solid #4a4948;"><div class="customListNoteDiv" style="">'+ progressNoteList[i].Notes+'</div></li>';

      var html = $('<div data-role="collapsible" id="ListItem" > </div>');
      html.append(innerHtml);
      $('#collapsiblesetForNotes').append(html);
      $('#collapsiblesetForNotes').collapsibleset('refresh');
		 }
		 }
  }
  
  function editNotePage(index){
 currentNote=progressNoteList[index].Notes;
 currentNoteAll=progressNoteList[index];
 isEditNote=true;
 $.mobile.changePage("#new-progress-notes-page");
 $("#progNotes").val(currentNote);
  }

  
  $("#submitProgNote").on("click",function(){
  var visitType=5;
  if(isInpatientOrder){
  visitType=3;
  }else{
  visitType=5;
  }
  if(isEditNote==false){
     var body = {
		VisitType: visitType,
		AdmissionNo: App.currentPatient.AdmissionNo,
		ProjectID:App.currentPatient.ProjectID,
		PatientTypeID:App.currentPatient.PatientType,
		PatientID:App.currentPatient.PatientID,
		ClinicID:App.user.ClinicID,
		Notes:$("#progNotes").val(),
		CreatedBy:App.user.DoctorID,
		EditedBy:App.user.DoctorID,
		NursingRemarks:" "
		
       };
      var request = new Req.RequestModel("CrProgressNotes", body, createProgressNotesSuccess, createProgressNotesError);
       Req.sendRequest(request);
	   }
	   else{
	   updateProgressNote(false,false);
	   }
  });

  function createProgressNotesSuccess(result){
  	   currentNote=$("#progNotes").val();
       App.showSuccessMsg(Loc.msg.successSave);
	   $("#verifyProgNote").attr('disabled',false);
	   isEditNote=true;
	     if(isInpatientOrder){
	 getInpatientOrders();
	 }else{
	 getProgressNote();
	 }
  }
  function createProgressNotesError(result){
   App.showErrorMsg(result.ErrorEndUserMessage);
  }
  function updateProgressNote(isVerify,isCancel){
  // if(val==true){
  // VerifiedNote=true;
  // }else{
   // VerifiedNote=false;
  // }
   var body = {
		ProjectID: App.user.ProjectID,
		//ClinicID:App.user.ClinicID,
		CreatedBy:App.user.DoctorID, 
		AdmissionNo:App.currentPatient.AdmissionNo, 
		LineItemNo:currentNoteAll.LineItemNo,
		Notes:$("#progNotes").val(),
		VerifiedNote:isVerify,
		CancelledNote:isCancel
		
       };
      var request = new Req.RequestModel("VerProgressNotes", body, updateProgressNotesSuccess, updateProgressNotesError);
       Req.sendRequest(request);
  }
 $("#cancelProgNote").on("click",function(){
      //$("#progNotes").val(currentNote);
	  isCancelledNote=true;
	  updateProgressNote(false,true);
 });
  
  $("#verifyProgNote").on("click",function(){
  isNoteVerified=true;
updateProgressNote(true,false);
   
 });
   function updateProgressNotesSuccess(result){
	   currentNote=$("#progNotes").val();
       App.showSuccessMsg(Loc.msg.successSave);
	   if(isNoteVerified==true){
	   isNoteVerified=false;
	   }
	   if(isCancelledNote==true){
	   isCancelledNote=false;
	   }
	    if(isInpatientOrder){
	 getInpatientOrders();
	 }else{
	 getProgressNote();
	 }
  }
  function updateProgressNotesError(result){
   App.showErrorMsg(result.ErrorEndUserMessage);
    isNoteVerified=false;
	isCancelledNote=false;
  }

 
  return {
    init: init,
	editNotePage:editNotePage,
	getProgressNote:getProgressNote,
	getInpatientOrders:getInpatientOrders
  }
})();