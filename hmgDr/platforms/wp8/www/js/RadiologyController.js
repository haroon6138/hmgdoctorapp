var RadiologyController = RadiologyController || {};
RadiologyController = (function () {

  var radOrderList=[];
  var RadOrderPage = "#rad-page";
  var RadDetailsPage = "#radDetails-page";
  var RadImageURL="";
  var  radIndex = 0;
//  var tempRadAppo=[];
  function init() {
   $(RadOrderPage).on("pagebeforeshow", function () {
      $("#radOrderList").empty();
      App.isUpdateEmail = false;
      if (radOrderList.length > 0) {
        DisplayRadOrderList();
      }
	   else {
         DisplayNoRecordMessage();
       }
    });
  } // init()
 function  setRadOrderList(radList){
 radOrderList=[];
 radOrderList=radList;
 $.mobile.changePage(RadOrderPage);
 }
  function retrieveRadError(response){
    App.showErrorMsg(response.ErrorEndUserMessage);
 }
 function DisplayRadOrderList(){
   var list = radOrderList;
   RadImageURL="";
    var dateAppo;
    $("#radOrderList").empty();
    var html = "";
	var clinicName="";
    for (var i = 0; i < list.length; i++) {
      var item = list[i];
	  if(item.ClinicName){
	  clinicName=item.ClinicName;
	  }else{
	  clinicName="";
	  }
	  var radDetails= "";
	  radDetails= item.ReportData;
      dateAppo = item.ReportDate;
      dateAppo = App.ConvertDate(dateAppo);
	  html += '<li>' + '<div "style="border:0px; background-color:white;"  onclick="RadiologyController.DisplayRadDetails(' + i + ')">' + '<table width="100%" border="0" cellspacing="3px"> <thead class="ui-widget-header"> </thead><tbody class="ui-widget-content">' + '<tr class="presAppoDetails" >' + '<td style="width:7%;vertical-align: top;" rowspan="3">' + '<div>' + '<img style="border:1px solid;margin-top: 3px;" width="70px" height="80px" src="' + item.DoctorImageURL + '" onError="this.onerror=null;this.src=\'' + 'img/dr_m.png' + '\';" align="middle" />' + '</div>' + '</td>' + '<td style="" colspan="2">' + '<div>' + '<div class="appoDrName">' + item.DoctorName + '</div>' + '<div style="font-size:12px; color:#ed1b2c;">' + clinicName + '</div>' + '<div>'+Loc.labPage.invoiceTitle+ item.InvoiceNo+'</div>'+'</div>' + '</td>' + '<td  rowspan="3" align="center" class="arrow-icon">' + '</td>' + '</tr>' + '<tr class="presAppoDetails" >' + '<td>' + '<img class="presSmallImage" src="img/calendar.png" width="30px" height="30px">'+ "  " + dateAppo + '</td>'  +'</tr> <tr class="presAppoDetails"> <td style="vertical-align: top;">' + '<img class="presSmallImage" style="top:-7px;" src="img/pin.png" width="30px" height="30px">' + "  " + item.ProjectName + '</td>' + '</tr>' + '</tbody></table>' + '</div>' + '</li>';
  
    //  html += '<li><div width="320px" style="border:1px solid #c74b45;background-color:white" onclick="RadiologyController.DisplayRadDetails(' + i + ')">' + '<table width="320px" border="0" cellspacing="3px"> <thead class="ui-widget-header"> </thead><tbody class="ui-widget-content">' + '<tr><td rowspan="4" align="center" width="90px"><img width="84px" height="120px" src="' + item.DoctorImageURL + '" onError="this.onerror=null;this.src=\'' + 'img/dr_m.png' + '\';" align="middle" />' + '</td>' + '<td><div class="appoTitle" style="font-size:14px;">' + Loc.labPage.DoctorName + ' ' + item.DoctorName + '</div>' + '<hr style="margin-top:10px;" class="splitter loginFormSplitter"></td>' + '<td rowspan="4" align="center" class="arrow-icon"  width="10px"></td>' + '</tr><tr><td><div class="appoDetails"><img class="app-cal-img" width="20px" height="20px" src="img/clock.png" />' + Loc.labPage.appoTitle + ' ' + dateFormat(dateAppo, "mm/dd/yyyy") + '</div>' + '</td></tr><tr><td><div class="appoDetails"><img class="app-invoice-img" width="20px" height="18px" src="img/invoice.png" />' + Loc.labPage.invoiceTitle + item.InvoiceNo + '</div>' + ' </td> </tr> <tr style="width:10%"> <td> <div class="appoDetails"> <img class="app-cal-img" width="20px" height="18px" src="img/Clinic.png">' + Loc.labPage.ClinicName + item.ClinicDescription + '</div></td></tr>' + '</tbody></table></div></li>';
    }
    $("#radOrderList").append(html).listview('refresh');
 }
 function DisplayRadDetails(index){
	var radItem=radOrderList[index];
	//tempRadAppo=radItem;
	radIndex=index;
	RadImageURL=radItem.ImageURL;
	var radDetails = document.getElementById('RadDetails');
	radDetails.innerHTML='';
	radDetails.innerHTML = radItem.ReportData;
	$.mobile.changePage(RadDetailsPage);
 }
  function DisplayNoRecordMessage() {
    $("#radOrderList").empty();
    var html = "";
    html = '<div id="NoDataDiv" width="320px" style="padding: 10px;border:1px solid #c74b45;background-color:white">'
    html += '<div>' + Loc.msg.NoDataYet + '</div>';
    html += '</div>';
    $('#radOrderList').append(html).listview('refresh');
  }
  return {
    init: init,
	setRadOrderList:setRadOrderList,
	DisplayRadDetails:DisplayRadDetails
  }
})();