var HomeController = HomeController || {};
HomeController = (function () {
  var homePageSel = "#home-page";

  function init() {
    $(homePageSel).on("pagebeforeshow", function () {
	$("#doctorPhoto").attr("src",App.user.DoctorImageURL);
	$("#doctorNameLink").text(App.user.DoctorName);
	
	if (App.LANG == "en") {
	$("#DoctorNameArrow").attr("src","img/leftPanelIcons/arrow.png");
	}else{
	$("#DoctorNameArrow").attr("src","img/leftPanelIcons/arrowFlipped.png");
	}
	});
	 $(document).on("pageinit", "#home-page", function () {    
 
        $( document ).on( "swipeleft swiperight", "#home-page", function( e ) {
            // We check if there is no open panel on the page because otherwise
            // a swipe to close the left panel would also open the right panel (and v.v.).
            // We do this by checking the data that the framework stores on the page element (panel: open).
            if ( $.mobile.activePage.jqmData( "panel" ) !== "open" ) {
                // if ( e.type === "swipeleft"  ) 
				// {
                    // $( "#rightCustomPanel" ).panel( "open" );
					
			
                // } 
				//else 
				if ( e.type === "swiperight" ) 
				{
                    $( "#leftCustomPanel" ).panel( "open" );
                }
            }
        });
		});
 
  } // init()
  return {
    init: init
  }
})();