//
//  settingsPlugin.h
//  HMG
//
//  Created by Admin on 7/24/13.
//
//

#import <Foundation/Foundation.h>
#import <Cordova/CDV.h>

@interface SettingsPlugin : CDVPlugin
{
}


- (void)changeLang:(CDVInvokedUrlCommand*)command;

@end
