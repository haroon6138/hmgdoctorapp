var SearchMedicineController = SearchMedicineController || {};
SearchMedicineController = (function () {
  var medicineName = '#searchMedicineInput';
  var searchBtSel = '#searchMedicine';
  var searchMedicinePageSel = "#searchMedicine-page";
  var medicineLocationPageSel = "#medicineLocation-page";
  var medicineList=[];
  var medicineLocationList=[];
  function init() {
  $(searchMedicinePageSel).on("pagebeforeshow", function () {
   $("#medicinesList").empty();
   $(medicineName).val(""); 
   $(searchBtSel).disableButton();
  });
   $(medicineLocationPageSel).on("pagebeforeshow", function () {
  var list =[];
  list=medicineLocationList;
    $("#medicineLocationList").empty();
	$("#medicineDetailesPrice").empty();
    var html = "";
	var Mhtml = "" ;
	
	Mhtml= '<p><span style="color:#ed1b2c;  font-weight: bold;">'+Loc.searchMedicinePage.Description+'</span>'+ list[0].ItemDescription +'</p>';
	Mhtml+= '<p><span style="color:#ed1b2c;  font-weight: bold;">'+Loc.searchMedicinePage.Price+'</span>'+ list[0].SellingPrice +'</p>';
    for (var i = 0; i < list.length; i++) {
      var item = list[i];
	   if(App.LANG == "en"){
     html += '<li><a href="#" class="patientListBtn ui-btn ui-shadow ui-corner-all ui-btn-icon-right ui-icon-next" onClick="SearchMedicineController.loadPharmacyMap('+i+')">'+item.LocationDescription+'</a> </li>'
   }else{
    html += '<li><a href="#" class="patientListBtn ui-btn ui-shadow ui-corner-all ui-btn-icon-left ui-icon-previous" onClick="SearchMedicineController.loadPharmacyMap('+i+')">'+item.LocationDescription+'</a> </li>'
   }
   }
   $("#medicineDetailesPrice").append(Mhtml);
    $("#medicineLocationList").append(html).listview('refresh');
	
	});
  $(medicineName).on("keyup", function () {
      validateSearchMedicineName();
    });
  } // init()
    function validateSearchMedicineName() {
    var nameVal = $(medicineName).val();
    if (nameVal != "" && nameVal.length >= 3) {
      $(searchBtSel).enableButton();
    } else {
      $(searchBtSel).disableButton();
    }
  }
   $(searchBtSel).on("click",function(){
   var nameVal = $(medicineName).val();
   var hasArabicChr=ValidationController.HasArabicCharacters(nameVal);
   var hasArabicNumbers=ValidationController.HasArabicNumbers(nameVal);
   if(!hasArabicChr && !hasArabicNumbers){
	searchMedicine();
   }else{
     alert(Loc.searchMedicinePage.notValidInput);
   }
   });
 function searchMedicine(){
  var body = {
  PHR_itemName:$(medicineName).val()
      };
      var request = new Req.RequestModel("searchMedicineName", body, searchMedicineSuccess, searchMedicineError);
      Req.sendRequest(request);
 }
 function searchMedicineSuccess(result){
 medicineList=[];
 medicineList=result.ListPharmcy;
  var list = result.ListPharmcy;
    $("#medicinesList").empty();
    var html = "";
    for (var i = 0; i < list.length; i++) {
      var item = list[i];
	  if(App.LANG == "en"){
     html += '<li><a href="#" class="patientListBtn ui-btn ui-shadow ui-corner-all ui-btn-icon-right ui-icon-next" onClick="SearchMedicineController.getMedicinePharmacies('+i+')">'+item.ItemDescription+'</a> </li>'
   }else{
    html += '<li><a href="#" class="patientListBtn ui-btn ui-shadow ui-corner-all ui-btn-icon-left ui-icon-previous" onClick="SearchMedicineController.getMedicinePharmacies('+i+')">'+item.ItemDescription+'</a> </li>'
   }
   }
    $("#medicinesList").append(html).listview('refresh');
 }
  function searchMedicineError(result){
  App.showErrorMsg(result.ErrorEndUserMessage);
 }
 function getMedicinePharmacies(index){
  var body = {
	ItemID:medicineList[index].ItemID
      };
      var request = new Req.RequestModel("getMedicineLocation", body, getMedicinePharmaciesSuccess, getMedicinePharmaciesError);
      Req.sendRequest(request);
 }
 function getMedicinePharmaciesError(result){
  App.showErrorMsg(result.ErrorEndUserMessage)
 }
 function getMedicinePharmaciesSuccess(result){
  medicineLocationList=[];
 medicineLocationList=result.PharmList;
 $.mobile.changePage("#medicineLocation-page");
 }
 function loadPharmacyMap(index){
 MapController.getMapLocation(index,medicineLocationList);
 }
  return {
    init: init,
	getMedicinePharmacies:getMedicinePharmacies,
	loadPharmacyMap:loadPharmacyMap
  }
})();