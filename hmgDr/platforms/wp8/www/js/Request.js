var Req = Req || {};
var reqCout = 0;
var wsUrl = "http://87.101.227.137:8090/"; //Online --------------------------
//var wsUrl = "http://10.10.94.101:1010/";  //Tamer
//var wsUrl = "http://10.10.94.85:1010/";  //Maha
//var wsUrl = "http://10.50.100.198:4444/";  
//var wsUrl = "http://10.50.100.196:4444/"; 
//var wsUrl='http://10.10.94.170:1111/' //jaber
var serviceLang = {};
serviceLang["en"] = 0;
serviceLang["ar"] = 1;
serviceLang["en"] = 2;
var services = {};
services["GtOutPatient"] = 'Services/DoctorApplication.svc/REST/GetMyOutPatient';
services["GtInPatient"] = 'Services/DoctorApplication.svc/REST/GetMyInPatient';
services["GtDischargePatient"] = 'Services/DoctorApplication.svc/REST/GtMyDischargePatient';
services["GtDisRefPatient"] = 'Services/DoctorApplication.svc/REST/GtMyDischargeReferralPatient';
services["GtDisRefdPatient"] = 'Services/DoctorApplication.svc/REST/GtMyDischargeReferredPatient';
services["GtReferralPatient"] = 'Services/DoctorApplication.svc/REST/GtMyReferralPatient';
services["GtReferredPatient"] = 'Services/DoctorApplication.svc/REST/GtMyReferredPatient';
services["GtTomPatient"] = 'Services/DoctorApplication.svc/REST/GtMyTomorrowPatient';
services["GtVitalSigns"] = 'Services/Doctors.svc/REST/Doctor_GetPatientVitalSign';//jaber
services["GtProgressNotes"]='Services/DoctorApplication.svc/REST/GetProgressNoteForInPatient';
services["CrProgressNotes"]='Services/DoctorApplication.svc/REST/CreateProgressNoteForInPatient';
services["VerProgressNotes"]='Services/DoctorApplication.svc/REST/UpdateProgressNoteForInPatient';
services["prescriptionOut"] = 'Services/DoctorApplication.svc/REST/GetPrescriptionReportByPatientID';
services["prescriptionIn"] = 'Services/DoctorApplication.svc/REST/GetPrescriptionReportForInPatient';
services["doctorSch"]='Services/Doctors.svc/REST/GetDoctorWorkingHoursTable';
services["labAppo"] = 'Services/DoctorApplication.svc/REST/GetPatientLabOreders';
services["labResList"] = 'Services/DoctorApplication.svc/REST/GetPatientLabResults';
services["searchMedicineName"] = 'Services/Lists.svc/REST/GetPharmcyItems';
services["getMedicineLocation"]='Services/Patients.svc/REST/GetPharmcyList';
services["getLocations"] = 'Services/Lists.svc/REST/Get_HMG_Locations';
services["projectsList"] = 'Services/Lists.svc/REST/GetProjectForDoctorAPP';
services["memberLogin"] = 'Services/Sentry.svc/REST/MemberLogIN';
services["smsAuth"] = 'Services/Sentry.svc/REST/MemberCheckActivationCode';
services["radiology"]='Services/DoctorApplication.svc/REST/GetPatientRadResult';
services["patientQues"]='Services/DoctorApplication.svc/REST/GtMyPatientsQuestions';
services["addresp"]='Services/DoctorApplication.svc/REST/AddDoctorResponse';
services["approvalOut"] = 'Services/Patients.svc/REST/GetApprovalStatus';
services["approvalIn"] = 'Services/DoctorApplication.svc/REST/GetApprovalStatusForInpatient';
services["frequencyList"]='Services/Lists.svc/REST/GetList_STPReferralFrequency';
services["referToDoctor"]='Services/DoctorApplication.svc/REST/ReferToDoctor';
services["addReferredDocRemark"]='Services/DoctorApplication.svc/REST/AddReferredDoctorRemarks';
services["VerifyRemarks"]='Services/DoctorApplication.svc/REST/VerifyReferralDoctorRemarks';
services["clinicProject"]='Services/DoctorApplication.svc/REST/GetClinicsByProjectID';
services["doctorClinic"]='Services/DoctorApplication.svc/REST/GetDoctorsByClinicID';
services["docProfile"]='Services/Doctors.svc/REST/GetDocProfiles';
services["bloodBank"]='Services/Patients.svc/REST/GetBloodGroup';
Req.token = "";
Req.RequestModel = function (ReqAction, ReqBody, success, error) {
  this.url = wsUrl + services[ReqAction];
  this.Body = ReqBody;
  this.Body.LanguageID = serviceLang[App.LANG];
  this.Body.stamp = new Date();
  this.Body.IPAdress = "11.11.11.11";
 this.Body.VersionID = 1.0;
  this.Body.Channel = 9;
  this.Body.TokenID = Req.token;
  this.Body.SessionID = App.token;
  this.Body.IsLoginForDoctorApp=true;
  this.success = success;
  this.error = error;
  if (App.isUserLogged) {
   if(App.user.ProjectID==2){
   this.Body.PatientOutSA=true;
   }else{
      this.Body.PatientOutSA=false;
   }
   if(App.currentPatient){
   this.Body.PatientTypeID = App.currentPatient.PatientType;
   }
}
}
Req.times = {};
Req.sendRequest = function (requestModel, silent) {
  console.log("\n\rRequest: " + requestModel.url + " " + $.toJSON(requestModel.Body));
  $.support.cors = true;
  $.mobile.allowCrossDomainPages = true;
  if (silent && silent == true) {} else {
    App.ShowUIBlocker(); //ph
    reqCout++;
  }
  var end = new Date();
  var dif = end - Req.start;
  var reqStart = new Date().getTime();
  //console.log("Start: " + end);
  // console.log("Start: " + dif);
  Req.start = new Date();
  var isErrorSubmitted = false;
  console.log(">> We are submitting a web service request ++++++++++++++");
  var jqxhr = $.ajax({
    type: 'POST',
    contentType: 'application/json',
    dataType: 'json',
    timeout: 60000,
    url: requestModel.url,
    data: $.toJSON(requestModel.Body),
    async: true
  }).success(function (data) {
    end = new Date();
    dif = end - Req.start;
    App.deviceInternetPresent = true;
    console.log("\n\Req Duration-" + requestModel.url + ": " + dateFormat(dif, "MM:ss") + "--->" + dif);
    console.log("\n\rsuccess:" + $.toJSON(data));
    try {
      $.when(App.checkNet()).done(function () {
        //console.log('Checking of internet conectivity is done...');
      });
    } catch (e) {
      App.deviceInternetPresent = false;
    }
    var result = data;
    if (result.MessageStatus == 1) {
      requestModel.success(result);
    } else if (result.IsAuthenticated == false) {
     // TimeOut or Token error happened. But lets distinguish private (HIS) login page with public login page.
        // $.growl({
          // title: Loc.msg.info,
          // message: Loc.msg.reloginMessage
        // });
        App.setUserSignOut();
       // $.mobile.changePage("#login-page");
    } else {
      // request error : result.MessageStatus == 2 (service error or message)
      var pageName = "N/A";
      var friendlyMsg = "Unknown Error";
      var formattedDisplayMessage = "N/A";
      var requestData = $.toJSON(requestModel.Body);
      var webServiceTrace = "N/A";
      requestModel.error(result);
      console.log("\n\r End User Msg: " + result.ErrorEndUserMessage);
      console.log("\n\r Technical Error Msg:" + result.ErrorMessage);
    }
    isErrorSubmitted = false; //restiing..
  }).error(function (jqXHR, textStatus, errorThrown) {
    if (isErrorSubmitted == false) {
      // console.log("\n\rOops!! Looks like we got an error. Constructing error message for submission...");
      var ErrMsg = Loc.msg.generalError; //"An error occurred while processing you request.";
      var ErrMsgTechnical;
      var ServerErrMsg = "N/A";
      var Isnetworking = "N/A";
      var IsnetworkingAlreadyChecked = false;
      var pageName = "N/A";
      var formattedDisplayMessage = "N/A";
      var requestData = $.toJSON(requestModel.Body);
      if (window.location.hash != null) {
        pageName = window.location.hash;
      } else {
        pageName = "N/A";
      }
      try {
        $.when(App.checkNet()).done(function () {
          if (App.deviceInternetPresent == false)
            Isnetworking = "No";
          else
            Isnetworking = "Yes";
        });
      } catch (e) {
        Isnetworking = "Unknown*";
      }
      try {
        ServerErrMsg = jQuery(jqXHR.responseText).text();
      } catch (e) {
        ServerErrMsg = "Unknown*"
      }
      if (textStatus == "" || textStatus == " ") {
        textStatus = "n/a"
      }
      if (errorThrown == "" || errorThrown == " ") {
        errorThrown = "n/a"
      }
      formattedDisplayMessage = "Web Service Returned [ " + textStatus + " - " + errorThrown + " - " + jqXHR.statusText + " (" + jqXHR.status + ") ]" + " \n ";
      //	1) Cyberroam login page in HMG, HMG user should log in to proxy fist.
      if (jqXHR.status == 200 && jqXHR.statusText == "Please login.") {
        IsnetworkingAlreadyChecked = true;
        ServerErrMsg = "Looks like your are Not logged on to Cyberroam in HMG";
        // console.log("\n\r" + formattedDisplayMessage);
        $.mobile.changePage("#no-Connection-page");
        // console.log("\n\rDisplay Friendly message to UI");
        //$.growl.notice({ message: "Testing: if(jqXHR.status == 200 && jqXHR.statusText == 'Please login.')" }); // for testing
      }else if (jqXHR.status == 404 || Isnetworking == "No"){
       $.mobile.changePage("#no-Connection-page");
	  // 2) Ajax response output says there is NO internet and the request has NOT been timeout
      }else if (jqXHR.status == 0 && Isnetworking == "No" && jqXHR.statusText != "timeout") {
        IsnetworkingAlreadyChecked = true;
        ServerErrMsg = "Looks like there is no internet connection detected.";
        //console.log("\n\r" + formattedDisplayMessage);
        $.mobile.changePage("#no-Connection-page");
		//console.log("\n\rDisplay Friendly message to UI");
        // $.growl.notice({ message: "Testing: if(jqXHR.status == 0 && Isnetworking == 'No' && jqXHR.statusText != 'timeout')" }); // for testing
      }
      // 3) Request has time out But internet seems to be available. 
      else if (jqXHR.status == 0 && jqXHR.statusText == "timeout" && Isnetworking == "Yes") {
        IsnetworkingAlreadyChecked = true;
        ServerErrMsg = "Server took too long to process your request.";
        //console.log("\n\r" + formattedDisplayMessage); console.log("\n\rDisplay Friendly message to UI");
        //App.supressUIBlockLayer = true;
        //App.lastErrorMessage = App.errorHandler(formattedDisplayMessage, requestData, ServerErrMsg, requestModel.url, pageName, 0);
        //App.showErrorMsg(Loc.msg.timeoutError); // note: dont display feedback to the user... SILENT POST...
        //$.growl.notice({ message: "Testing: if(jqXHR.status == 0 && jqXHR.statusText == 'timeout' && Isnetworking == 'Yes')" }); // for testing
      }
      // 4) Request has timeout And also Internet is NOT available, so we cant log exceptions.
      else if (jqXHR.status == 0 && jqXHR.statusText == "timeout" && Isnetworking == "No") {
        IsnetworkingAlreadyChecked = true;
        ServerErrMsg = "Server took too long to process your request.";
        //console.log("\n\r" + formattedDisplayMessage); console.log("\n\rDisplay Friendly message to UI");
        App.showErrorMsg(Loc.msg.timeoutError);
        //$.growl.notice({ message: "Testing: if(jqXHR.status == 0 && jqXHR.statusText == 'timeout' && Isnetworking == 'No')" }); // for testing
      }
      // 5) Net is AVAILABLE and above special cases did not occur, so we going proceed to log exception
      else if (Isnetworking == "Yes") {
        //console.log("\n\r Seems like internet is present So... Submission of error message to server...");
        //App.lastErrorMessage = App.errorHandler(formattedDisplayMessage, requestData, ServerErrMsg, requestModel.url, pageName, 0);
        //console.log("\n\r" + formattedDisplayMessage); console.log("\n\rDisplay Friendly message to UI");
        $.growl.notice({
          message: Loc.errorDisplayPage.title + ". " + Loc.errorDisplayPage.titleDetails
        }); // for testing
        //	App.showErrorMsg(Loc.errorDisplayPage.title + "." + Loc.errorDisplayPage.titleDetails);
      }
    } else {
      isErrorSubmitted = true;
      //	console.log("\n\rSeems like the server is down or has problems, so we dont log exceptions until server is alive.");
    }
    isErrorSubmitted = true;
  }).complete(function () {
    var elapsed = dateFormat(dif, "MM:ss") + ":" + dif;
    App.lastAccessedWebService = requestModel.url + " - " + dateFormat(dif, "MM:ss") + "---> " + elapsed + " [" + dateFormat(end, "longTime", false) + "]";
    App.lastErrorMessage = "";
    var reqEnd = new Date().getTime();
    var reqTime = reqEnd - reqStart;
    // console.log('\n\Elapsed Request Time: --------> ' + App.millisecondsToString(reqTime));
    if (silent && silent == true) {} else {
      reqCout--;
    }
    // if (reqCout == 0) {
      // // $(".top-logo-loader").hide();
      // // $(".top-logo").show();
      // // if (App.LANG == "en") {
        // // $(".top-logo-text.ui-btn-up-a").css('margin-right', '75px');
      // // } else {
        // // $(".top-logo-text.ui-btn-up-a").css('margin-right', '5px');
      // // }
    // }
  });
}