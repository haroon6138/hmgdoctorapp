var DrProfileController = DrProfileController || {};
DrProfileController = (function () {
  var staffPageSel = "#staff-page";
  var $staffList = $("#staffList");
  var staffList = [];
  var drProfilePageSel = "#drProfile-page";
  var tmpDrProfile = {};
  var tmpRate = 0;
  var drIndexI;
  var drIndexJ;
  var doctorRatingList = [];
  var imgsArray = ["img/dr_m.png", "img/dr_f.png"];

  function init() {
 
    $(drProfilePageSel).on("pagebeforeshow", function () {
	tmpDrProfile=App.user;
	if(tmpDrProfile){
      $("#drProfileDisplayImg").empty();
      $("#drProfileProf").empty();
      $("#drProfileNat").empty();
      $("#drProfileDisplayImg").append('<img width="80px" height="90px"   style="border-radius: 150px; box-shadow: 0 0 8px rgba(0, 0, 0, .8); -webkit-box-shadow: 0 0 8px rgba(0, 0, 0, .8); -moz-box-shadow: 0 0 8px rgba(0, 0, 0, .8);" class="glossy-reflection" src="' + tmpDrProfile.DoctorImageURL + '">');
      var profileData = ' ' + Loc.drProfilePage.Speciality + ' ';
      if (typeof tmpDrProfile.Specialty !== 'undefined' && tmpDrProfile.Specialty !== null) {
        for (var i = 0; i < tmpDrProfile.Specialty.length; i++) {
          profileData += tmpDrProfile.Specialty[i] + ", ";
        }
      } else {
        profileData = "";
      }
      if (tmpDrProfile.NationalityName != null) {
        $("#drProfileNat").append('<div class="nat-img" style="margin-left: 30%;"><img  src="' + tmpDrProfile.NationalityFlagURL + '"  class="nat-flag">&nbsp;<span class="nat-name drs-details">' + tmpDrProfile.NationalityName.trim() + '</span></div>');
      }
      $("#drProfileName").text(tmpDrProfile.Title_Description + ' ' + tmpDrProfile.DoctorName);
      if (tmpDrProfile.Gender_Description != null) {
        $("#drProfileGender").html('<img src="img/DrProfileIcons/gender.png" class="doctorProfileIcons" style=""/><span style="color: red; margin-bottom: auto; margin-top: auto;">' + Loc.drProfilePage.Gender + '</span> <span class="">' + tmpDrProfile.Gender_Description + '</span>');
      }
      $("#drProfileDept").html('<img src="img/loginScreen/clinic.png" class="doctorProfileIcons" style=""/><span style="color: red;">' + Loc.drProfilePage.Clinic + '</span> <span style="text-transform: capitalize !important;">' + tmpDrProfile.ClinicDescription + '</span>');
      if (tmpDrProfile.ProjectName != null) {
        $("#drProfileProj").html('<img src="img/loginScreen/project.png" class="doctorProfileIcons" style=""/><span style="color: red;">' + Loc.drProfilePage.Hospital + '</span>  <span class="">' + tmpDrProfile.ProjectName + '</span>');
      }
      profileData = profileData.replace(/, $/, ".")
      $("#drProfileDesc").html('<img src="img/DrProfileIcons/spiciality.png" class="doctorProfileIcons" style=""/><span class="nat-flag" style=""> <span style="color: red;">' + profileData.substring(0, profileData.indexOf(":")+1) +'</span>'+ profileData.substring(profileData.indexOf(":")+1)+'</span>');
      var x = tmpDrProfile.DoctorProfileInfo;
      if (x != null) {
        x = x.replace("\r\n", "<br>");
        x = x.replace("\n", "<br>");
        $("#drProfileProf").append(x);
      }
	  }
    });
	    $(drProfilePageSel).on("pageshow", function () {
			var el = document.querySelector('.nat-name');
		el.innerHTML = el.innerHTML.replace(/&nbsp;/g,'');
		});
  } // init()
  return {
    init: init
  }
})();