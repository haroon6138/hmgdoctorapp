var MyPatientsController = MyPatientsController || {};
MyPatientsController = (function () {
  var searchPatientResults="#my-patients-page";
   var searchPatientsPage="#search-patient-page";
  var patientType='1';
  var isQrPatient=false;
  var isHomeOutpatientBtn=false;
var patientList=[];
  function init() {
	 $(searchPatientResults).on("pagebeforeshow", function () {
	 App.profilePageIndex=0;
	 displayPaitentsSearchResults();
});
 $(searchPatientResults).on("pagehide", function () {
//$("#out-radio-choice-h-2c").attr("checked", "checked").checkboxradio("refresh");
$("input[name*=period-radio-outpatient]").prop("checked", false).checkboxradio("refresh");
$("#out-radio-choice-h-2c").prop("checked", true).checkboxradio("refresh");
});
	 $(searchPatientsPage).on("pagehide", function () {

$('#fNameInput').val("");
$('#mNameInput').val("")
$('#lNameInput').val("")
$('#mNumberInput').val("")
$('#patientIDNInput').val("")
$('#PatientIDInput').val("");
$('#fDateInput').val("");
$('#tDateInput').val("");
$('#onlyArrivedPt').attr("checked",false).checkboxradio("refresh");
});

  } // init()
  $("#patient-type").on("change",function(){
  if($("#patient-type").val()=='2' || $("#patient-type").val()=='4'|| $("#patient-type").val()=='5')
  {
    $(".hideElements").show();
  $(".dateTr").hide();
  $(".customRedCheckBox").hide();
  $("#onlyArrivedPt").hide();
  }
  else  if($("#patient-type").val()=='3' || $("#patient-type").val()=='6' || $("#patient-type").val()=='7'){
      $(".hideElements").show();
  $(".dateTr").show();
  $(".customRedCheckBox").hide();
  $("#onlyArrivedPt").hide();
  }
  else if($("#patient-type").val()=='7'){
  $(".hideElements").hide();
  }
  else{
      $(".hideElements").show();
   $(".dateTr").show();
  $(".customRedCheckBox").show();
  $("#onlyArrivedPt").show();
  $('#onlyArrivedPt').attr("checked",false).checkboxradio("refresh");
  }
  });
  $('#searchPatientBtn').on("click",function(){
  App.setBackPage("#searchResultBackBtn","#search-patient-page");
 var n= $("#patient-type").val();
 var methodToCall="";
 //var doctorID=0;
  switch(n)
{
case '1':
 methodToCall="GtOutPatient";
 patientType='1';
 //doctorID=1485;
  break;
case '2':
  methodToCall="GtInPatient";
   patientType='2';
   // doctorID=1140;
  break;
case '3':
  methodToCall="GtDischargePatient";
   patientType='3';
  // doctorID=App.user.DoctorID;
  break;
case '4':
  methodToCall="GtReferralPatient";
   patientType='4';
  // doctorID=App.user.DoctorID;
  break;
  case '5':
  methodToCall="GtReferredPatient";
   patientType='5';
  // doctorID=App.user.DoctorID;
  break;
case '6':
  methodToCall="GtDisRefPatient";
   patientType='6';
  // doctorID=App.user.DoctorID;
  break;
  // case '7':
  // methodToCall="GtDisRefdPatient";
   // patientType='7';
   // doctorID=App.user.DoctorID;
  // break;
 case '8':
  methodToCall="GtTomPatient";
   patientType='8';
   //doctorID=App.user.DoctorID;
  break;
default:
 methodToCall="GtOutPatient";
  patientType='1';
 // doctorID=App.user.DoctorID;
}

      var body = {
        ProjectID: App.user.ProjectID,
		ClinicID:App.user.ClinicID,
		//ClinicID:0,
		DoctorID:App.user.DoctorID,
		//DoctorID:doctorID,
		PatientID:parseInt($("#PatientIDInput").val()),
		FirstName:$('#fNameInput').val(),
		MiddleName:$('#mNameInput').val(),
		LastName:$('#lNameInput').val(),
		PatientIdentificationID:$('#patientIDNInput').val(),
		PatientMobileNumber:$('#mNumberInput').val(),
		From:$('#fDateInput').val(),
		To:$('#tDateInput').val(),
		ISJustArrivedPatients:($("#onlyArrivedPt").is(':checked')) ? true : false,
		PatientOutSA:($("#patient-loc").val()==='true')
      };
	  body= validateSearchOptions(body);
      var request = new Req.RequestModel(methodToCall, body, retrievePatientSuccess, retrievePatientError);
      Req.sendRequest(request);
  });
    function readPatientQrSuccess(result){
	  isHomeOutpatientBtn=false;	
    patientList=[];
    switch(patientType)
{
case '1':
  patientList=result.List_MyOutPatient;
  break;
case '2':
  patientList=result.List_MyInPatient;
default:
 patientList=result.List_MyInPatient;
	}
	isQrPatient=true;
	goToPatientProfile(0);
}
  function retrievePatientSuccess(result){
  isQrPatient=false;
  isHomeOutpatientBtn=false;	
    patientList=[];
    switch(patientType)
{
case '1':
  patientList=result.List_MyOutPatient;
  break;
case '2':
  patientList=result.List_MyInPatient;
  break;
case '3':
  patientList=result.List_MyDischargePatient;
  break;
case '4':
  patientList=result.List_MyReferralPatient;
  break;
  case '5':
  patientList=result.List_MyReferredPatient;
  break;
// case '6':
  // patientList=result.List_MyDischargeReferralPatient;
  // break;
  // case '7':
  // patientList=result.List_MyDischargeReferredPatient;
  // break;
 case '8':
  patientList=result.List_MyTomorrowPatient;
  break;
default:
 patientList=result.List_MyOutPatient;
}
// if(patientType=='1'){
// $.mobile.changePage(
    // "#my-patients-page",
  // { allowSamePageTransition : true,
      // transition              : 'none',
      // showLoadMsg             : false}
  // );}else{
  if ($.mobile.activePage.is("#my-patients-page") ){
   displayPaitentsSearchResults();
  }else{
   $.mobile.changePage("#my-patients-page"); 
  }

 //}
  }
  function retrievePatientError(result){
  patientList=[];
  $("#searchPatientsList").empty();
   if( isHomeOutpatientBtn==true){
   isHomeOutpatientBtn=false;
   	/*******clear filter********************/
	    $('input[data-type="search"]').val("");
    $('input[data-type="search"]').trigger("keyup");
		/*******clear filter********************/
    $.mobile.changePage("#my-patients-page"); 
   }else{
    App.showErrorMsg(result.ErrorEndUserMessage);
   }
  }
  function getMyOutpatients(){
  patientType='1';
  var obj={}
   obj.fromDate="0";
  obj.toDate="0";
     var checked = $("input[name*=period-radio-outpatient]:checked").val();
	 if(checked=='d'){
	 obj=calculateSearchPeriod(0,'d');
	}else if(checked=='t'){
	 obj=calculateSearchPeriod(1,'t');
	 }else if(checked=='w'){
	 obj=calculateSearchPeriod(7,'w');
	 }else{
	 obj=calculateSearchPeriod(7,'w');
	 }
 isHomeOutpatientBtn=true;	 
	
   var body = {
        ProjectID: App.user.ProjectID,
        ClinicID:App.user.ClinicID,
		DoctorID:App.user.DoctorID
		// ClinicID:0,
		// DoctorID:1485
		//From: obj.fromDate,
      //  To: obj.toDate
      };
	 body= validateSearchOptions(body);
	 body.From=obj.fromDate;
	 body.To=obj.toDate;
      var request = new Req.RequestModel("GtOutPatient", body, retrievePatientSuccess, retrievePatientError);
      Req.sendRequest(request);
  }
function getScannedPatientID(projectID,patientID,type){
var requestLink="";
if(type==1){
patientType='2';
requestLink="GtInPatient"
}else{
requestLink="GtOutPatient"
patientType='1';
}
   var body = {
       ProjectID: parseInt(projectID),
		ClinicID:0,
		DoctorID:0,
		PatientID:parseInt(patientID)
      };
	  body= validateSearchOptions(body);
	  body.PatientID=parseInt(patientID);
	  body.ProjectID=parseInt(projectID);
	   body.From="0";
	 body.To="0";
      var request = new Req.RequestModel(requestLink, body, readPatientQrSuccess, retrievePatientError);
      Req.sendRequest(request);
}
    function getMyInpatients(){
  patientType='2';
   var body = {
       ProjectID: App.user.ProjectID,
		ClinicID:App.user.ClinicID,
		DoctorID:App.user.DoctorID
		// ProjectID: 0,//to be changed
		// ClinicID:0,//to be changed
		// DoctorID:1140//to be changed
      };
	  body= validateSearchOptions(body);
      var request = new Req.RequestModel("GtInPatient", body, retrievePatientSuccess, retrievePatientError);
      Req.sendRequest(request);
  }
     function getDischargedPatients(){
  patientType='3';
   var body = {
 // ProjectID: 0,
		// ClinicID:0,
		// DoctorID:70184
		ProjectID: App.user.ProjectID,
		ClinicID:App.user.ClinicID,
		DoctorID:App.user.DoctorID
      };
	  body= validateSearchOptions(body);
      var request = new Req.RequestModel("GtDischargePatient", body, retrievePatientSuccess, retrievePatientError);
      Req.sendRequest(request);
  }
     function getReferrals(){
  patientType='4';
   var body = {
        //ProjectID: 12,
		ProjectID:App.user.ProjectID,
		//ClinicID:0,
		ClinicID:App.user.ClinicID,
		//DoctorID:41
		DoctorID:App.user.DoctorID
      };
	  body= validateSearchOptions(body);
      var request = new Req.RequestModel("GtReferralPatient", body, getReferralsSuccess, getReferralsError);
      Req.sendRequest(request);
  }
  function getReferred(){
    patientType='5';
   var body = {
        ProjectID: App.user.ProjectID,
		ClinicID:App.user.ClinicID,
		DoctorID:App.user.DoctorID
		// DoctorID:1639,
		// ClinicID:0,
		// ProjectID: 12,
		
      };
	  body= validateSearchOptions(body);
      var request = new Req.RequestModel("GtReferredPatient", body, getReferralsSuccess, getReferralsError);
      Req.sendRequest(request);
  }
    function getDischargedReferrals(){
  patientType='6';
   var body = {
        ProjectID: App.user.ProjectID,
		ClinicID:App.user.ClinicID,
		DoctorID:App.user.DoctorID
		 // ProjectID: 12,
		// ClinicID:0,
		// DoctorID:  1482  
      };
	  body= validateSearchOptions(body);
	  // body.From= "2013-03-30 15:44:00",
         // body.To="2013-05-07 16:29:00"
      var request = new Req.RequestModel("GtDisRefPatient", body, getReferralsSuccess, getReferralsError);
      Req.sendRequest(request);
  } 
  function getDischargedReferred(){
  patientType='7';
   var body = {
        // ProjectID: 12,
		// ClinicID:0,
		// DoctorID:1871
		ProjectID: App.user.ProjectID,
		ClinicID:App.user.ClinicID,
		DoctorID:App.user.DoctorID
      };
	  body= validateSearchOptions(body);
	  // body.From="2013-03-30 15:44:00"
	  // body.To="2013-05-07 16:29:00"
      var request = new Req.RequestModel("GtDisRefdPatient", body, retrievePatientSuccess, retrievePatientError);
      Req.sendRequest(request); 
  }
    function getTomorrowPatients(){
  patientType='8';
   var body = {
        ProjectID: App.user.ProjectID,
		ClinicID:App.user.ClinicID,
		DoctorID:App.user.DoctorID
      };
	  body= validateSearchOptions(body);
      var request = new Req.RequestModel("GtTomPatient", body, retrievePatientSuccess, retrievePatientError);
      Req.sendRequest(request);
  }
function getReferralsSuccess(result){
var refList=[];
var headerText="";
var isReferredList=false;
var isDischargedReferral=false;
switch(patientType)
{
case '4':
  refList=result.List_MyReferralPatient;
  headerText=Loc.referralPage.myRefPat;
  break;
  case '5':
  refList=result.List_MyReferredPatient;
  isReferredList=true;
  headerText=Loc.referralPage.myRefrdPat;
  break;
case '6':
  refList=result.List_MyDischargeReferralPatient;
  headerText=Loc.referralPage.disRefPat;
  isDischargedReferral=true;
  break;
  // case '7':
  // refList=result.List_MyDischargeReferredPatient;
  // headerText=Loc.referralPage.disRefrdPat;
  // break;
default:
 refList=[];
 headerText=Loc.referralPage.myRefPat;
}  
document.getElementById("referListHeader").innerHTML = headerText;
ReferralController.displayReferrals(refList,isReferredList,isDischargedReferral);
}
function getReferralsError(response){
App.showErrorMsg(response.ErrorEndUserMessage);
}
   function displayPaitentsSearchResults(){
    var html="";
	$("#searchPatientsList").empty();
	 $("#search-patientRadio").show();
	/*******clear filter********************/
	    $('input[data-type="search"]').val("");
    $('input[data-type="search"]').trigger("keyup");
		/*******clear filter********************/
	//$("#search-patient-content-radioButton").empty();

	//alert(patientList.length);
	var OutPhtml ='';
	  
	 for(var i=0;i<patientList.length;i++){
	
	 if(App.LANG == "en"){
	 html+='<li><a href="#" class="patientListBtn ui-btn ui-shadow ui-corner-all ui-btn-icon-right ui-icon-next" onClick="MyPatientsController.goToPatientProfile('+i+')">'
			 }else{
	  html+='<li><a href="#" class="patientListBtn ui-btn ui-shadow ui-corner-all ui-btn-icon-left ui-icon-previous" onClick="MyPatientsController.goToPatientProfile('+i+')">'
	 }
	 html+='<h2>'+patientList[i].FirstName+" " +patientList[i].LastName+'</h2>'
    +'<p><span style="color:#ed1b2c;">'+Loc.patientSearchResult.fileNo+'</span>'+patientList[i].PatientID+'</p>';
	var admissionDate = new Date();
	  if(patientType =='2' || patientType =='3'|| patientType =='4'|| patientType =='5'|| patientType =='6' || patientType =='7'){
	   admissionDate= patientList[i].AdmissionDate ;
	   //App.ConvertDate(patientList[i].AdmissionDate);  ///need it as DateTime to use it in the calculation function
	  admissionDate = new Date(parseInt(admissionDate.substring(6, admissionDate.length - 2)));
	 html+='<p><span style="color:#ed1b2c;">'+Loc.patientSearchResult.AdmissionDate+'</span>'+ dateFormat(admissionDate, "d/mm/yyyy") +'</p>';
	  }
	  
	  /*********************************Shaden's edit START********************************/
	  if(patientType =='2' || patientType =='4'|| patientType =='5'){
	     var TodayDate = new Date();
		 var numberOfDays = 0;
		 numberOfDays= App.calculateInpatientStayPeriod( admissionDate , TodayDate);
		 html+='<p><span style="color:#ed1b2c;">'+Loc.patientSearchResult.StayDays+'</span>'+numberOfDays+'</p>';
	  }
	  
	  if(patientType =='3' || patientType =='6' || patientType =='7'){
	  var dischargeDate = patientList[i].DischargeDate;
	  //App.ConvertDate(patientList[i].DischargeDate); ///need it as DateTime to use it in the calculation function
	  var numberOfDays = 0;
	  if(dischargeDate){
	  dischargeDate = new Date(parseInt(dischargeDate.substring(6, dischargeDate.length - 2)));
	  numberOfDays= App.calculateInpatientStayPeriod( admissionDate , dischargeDate);
	  dischargeDate=dateFormat(dischargeDate, "d/mm/yyyy");
	  }
	  
	 html+='<p><span style="color:#ed1b2c;">'+Loc.patientSearchResult.DischargeDate+'</span>'+dischargeDate +'</p>';
	 html+='<p><span style="color:#ed1b2c;">'+Loc.patientSearchResult.StayDays+'</span>'+numberOfDays +'</p>';
	  }
	  
	  if(patientType =='8' || patientType =='1'){
	  var AppointmentTime = patientList[i].StartTime;
	  if(AppointmentTime){
	  AppointmentTime= AppointmentTime.toString();
	  AppointmentTime= AppointmentTime.substring(0,5);
	  }
	  var AppointmentDate=App.ConvertDate(patientList[i].AppointmentDate);
	  html+= '<p><span style="color:#ed1b2c;">'+Loc.patientSearchResult.AppointmentDate+'</span>'+ AppointmentDate +'</p>'+'<p><span style="color:#ed1b2c;">'+Loc.patientSearchResult.AppointmentTime+'</span>'+ AppointmentTime +'</p>';
	  }
	  
	  if(patientType =='1'){
	 $("#search-patientRadio").show();
	  }
	  else{
	  $("#search-patientRadio").hide();
	  }
	  	  /*********************************Shaden's edit END********************************/
	  html+='</a> </li>';
	 }
	 $('#searchPatientsList').append(html);
	 $('#searchPatientsList').listview('refresh');
  }
  function validateSearchOptions(bd){
		
 if($('#fNameInput').val()){
bd.FirstName=$('#fNameInput').val();
 }else{
bd.FirstName="0";
 }
 if($('#mNameInput').val()){
	bd.MiddleName=$('#mNameInput').val();
 }else{
 	bd.MiddleName="0";
 }
  if($('#lNameInput').val()){
 	bd.LastName=$('#lNameInput').val();
 }else{
 	bd.LastName="0";
 }
  if($('#mNumberInput').val()){
 bd.PatientMobileNumber=$('#mNumberInput').val();
 }else{
 bd.PatientMobileNumber="0";
 }
  if($('#patientIDNInput').val()){
	bd.PatientIdentificationID=$('#patientIDNInput').val();
 }else{
 	bd.PatientIdentificationID="0";
	}
  if($('#PatientIDInput').val()){
  bd.PatientID=parseInt($('#PatientIDInput').val());
 }else{
   bd.PatientID=0;
 }
  if($('#fDateInput').val()){
bd.From=$('#fDateInput').val();
 }else{
 	  if( patientType=='1'){
	  obj=calculateSearchPeriod(7,'w');
	   bd.From=obj.fromDate;
	  }else{
 	bd.From="0";
	}
 }
  if($('#tDateInput').val()){
bd.To=$('#tDateInput').val();
 }else{
 	  if( patientType=='1'){
	  obj=calculateSearchPeriod(7,'w');
	 bd.To=obj.toDate;
	  }else{
 		bd.To="0";
		}
 }
 return bd;
  }
  function goToPatientProfile(index){
  if(isQrPatient==true)
  App.setBackPage("#profileBackKey","#home-page");
  else
  App.setBackPage("#profileBackKey","#my-patients-page");
  var obj=patientList[index];
      obj.isDischarged=false;
  if(patientType =='2' || patientType =='3'|| patientType =='4'|| patientType =='5'|| patientType =='6' || patientType =='7'){
  obj.PatientStayType=1
  obj.AdmissionNo=parseInt(obj.AdmissionNo);
 } else{
  obj.PatientStayType=2
  }
if(patientType=='3'||patientType=='6'|| patientType=='7'){
obj.isDischarged=true;
}
  App.currentPatient=obj;
  PatientProfileController.setPatientProfile(obj);
 }
 
  function goToPatientPrescription(index){
  var obj=patientList[index];
  PrescriptionController.setPatientInfo(obj);
 }
 function calculateSearchPeriod(days,type){
 var obj={};
 if(type=='t')
 obj.fromDate= new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
 else{
 obj.fromDate=new Date();
 }
 obj.fromDate = dateFormat(obj.fromDate, "yyyy-mm-dd");
 obj.toDate=new Date();
 obj.toDate.setDate(obj.toDate.getDate()+days);
 obj.toDate = dateFormat(obj.toDate, "yyyy-mm-dd");
 obj.fromDate = obj.fromDate.toString();
 obj.toDate = obj.toDate.toString();
 return obj;
 }

  return {
    init: init,
	goToPatientProfile:goToPatientProfile,
	goToPatientPrescription:goToPatientPrescription,
	getMyOutpatients:getMyOutpatients,
	getMyInpatients:getMyInpatients,
	getReferrals:getReferrals,
	getDischargedPatients:getDischargedPatients,
	getDischargedReferrals:getDischargedReferrals,
	getTomorrowPatients:getTomorrowPatients,
	getDischargedReferred:getDischargedReferred,
	getReferred:getReferred,
	getScannedPatientID:getScannedPatientID
  }
})();