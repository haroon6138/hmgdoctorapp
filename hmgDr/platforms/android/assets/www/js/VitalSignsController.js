var VitalSignsController = VitalSignsController || {};
VitalSignsController = (function () {
var vitalSignsList=[];
var vitalSignsDetails;
  var imgsArray = ["img/dr_m.png", "img/dr_f.png"];
  function init() {
     $("#vitalSigns-page").on("pagebeforeshow", function () {
      var list=vitalSignsList;
	  $("#vitalSingsList").empty();
	 var  html="";
	  if(list){
	  for(var i=0;i<list.length;i++){
	 var vtDate= App.ConvertDate(list[i].CreatedOn);
	 var doctorName = (list[i].DoctorName) ? list[i].DoctorName : "";
	 var ClinicName  = (list[i].ClinicName) ? list[i].ClinicName : "";
	//vtDate= dateFormat(vtDate, "dd/mm/yyyy");
	if(App.LANG == "en"){
		  html+='<li><a href="#" class="patientListBtn ui-btn ui-shadow ui-corner-all ui-btn-icon-right ui-icon-next" onClick="VitalSignsController.displayVsDetails('+i+')">'
				 }else{
				   html+='<li><a href="#" class="patientListBtn ui-btn ui-shadow ui-corner-all ui-btn-icon-left ui-icon-previous" onClick="VitalSignsController.displayVsDetails('+i+')">'
				 }
				 if(doctorName && ClinicName){
				 html+='<table><tr><td style="width:7%;vertical-align: top;" rowspan="3"><img style="border:1px solid;margin-top: 3px;" width="70px" height="80px" src="' +list[i].DoctorImageURL+ '" onError="this.onerror=null;this.src=\'' + imgsArray[0] + '\';" align="middle" /></td>';
				 html+='<td><h2>'+doctorName+'</h2></td></tr>';
				 html+='<tr><td><p style="color:#ed1b2c;">'+ClinicName+'</p></td></tr><tr><td>'
			+'<p>'+'<span style="color:#ed1b2c;">'+Loc.vitalSignsPage.date+" </span>"+vtDate+' - '+'<span style="color:#ed1b2c;">'+Loc.vitalSignsPage.time+" " +'</span>'+list[i].ActualTimeTaken+'</p></a> </li></td></tr></table>';
				 }else{
			html+='<h2>'+Loc.vitalSignsPage.date+" "+vtDate+'</h2>'
			+'<p><span style="color:#ed1b2c;">'+Loc.vitalSignsPage.time+" " +'</span>'+list[i].ActualTimeTaken+'</p></a> </li>';
	  }
	  }
	  $("#vitalSingsList").append(html);
	   $("#vitalSingsList").listview("refresh");
	  }
    });
	
	 $("#vitalSignsDetails-page").on("pagebeforeshow", function () {
	
	$("#generalMesurmentList> li>span").empty();
	if(vitalSignsDetails){
	$("#weightVal").append(vitalSignsDetails.WeightKg);
	$("#heightVal").append(vitalSignsDetails.HeightCm);
	$("#bmiVal").append(vitalSignsDetails.BodyMassIndex);
	$("#HeadCircumVal").append(vitalSignsDetails.HeightCm);
	$("#IBWLbsVal").append(vitalSignsDetails.IdealBodyWeightLbs);
	$("#LBWLbsVal").append(vitalSignsDetails.LeanBodyWeightLbs);
	$("#gcsVal").append(vitalSignsDetails.GCScore);
	$("#triageCatgVal").append(vitalSignsDetails.TriageCategory);
	$("#generalMesurmentList").listview("refresh");
	 
	$("#temperatureList> li>span").empty();
	$("#temperatureCVal").append(vitalSignsDetails.TemperatureCelcius);
	$("#temperatureMethodVal").append(vitalSignsDetails.TemperatureCelciusMethodDesc);
    $("#temperatureList").listview("refresh");

	$("#pulseList> li>span").empty();
	$("#pulseMsrVal").append(vitalSignsDetails.PulseBeatPerMinute);
	$("#rhythmVal").append(vitalSignsDetails.PulseRhythmDesc);
    $("#pulseList").listview("refresh");

	$("#respirationList> li>span").empty();
	$("#respVal").append(vitalSignsDetails.RespirationBeatPerMinute);
	$("#patternVal").append(vitalSignsDetails.RespirationPatternDesc);
    $("#respirationList").listview("refresh");

	$("#bloodPressureList> li>span").empty();
	$("#bdPressureLowerVal").append(vitalSignsDetails.BloodPressureLower);
	$("#bdPressureHigherVal").append(vitalSignsDetails.BloodPressureHigher);
	$("#patientPosVal").append(vitalSignsDetails.BloodPressurePatientPositionDesc);
	$("#cuffLocVal").append(vitalSignsDetails.BloodPressureCuffLocationDesc);
	$("#cuffSizeVal").append(vitalSignsDetails.BloodPressureCuffSizeDesc);
    $("#bloodPressureList").listview("refresh");

	$("#oxygList> li>span").empty();
	$("#oxsoVal").append(vitalSignsDetails.SAO2);
	$("#oxfiVal").append(vitalSignsDetails.FIO2);
    $("#oxygList").listview("refresh");

	$("#painScaleList> li>span").empty();
	$("#painScaleVal").append(vitalSignsDetails.PainScore);
	$("#PainLocVal").append(vitalSignsDetails.PainLocation);
	$("#PainChVal").append(vitalSignsDetails.PainCharacter);
	$("#painDurVal").append(vitalSignsDetails.PainDuration);
	$("#painAssmVal").append(vitalSignsDetails.IsPainManagementDone);
	$("#painfrqVal").append(vitalSignsDetails.PainFrequency);
	$("#PainScoreDescVal").append(vitalSignsDetails.PainScoreDesc);
    $("#painScaleList").listview("refresh");
	}
    });
  } // init()
  function setVitalSigns(list) {
  vitalSignsList=list;
    	 
	  $.mobile.changePage("#vitalSigns-page"); 
  }
  function displayVsDetails(index){
  vitalSignsDetails=vitalSignsList[index];
   $.mobile.changePage("#vitalSignsDetails-page"); 
  }

  return {
    init: init,
	setVitalSigns:setVitalSigns,
	displayVsDetails:displayVsDetails
  }
})();