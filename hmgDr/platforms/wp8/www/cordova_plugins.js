cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/org.apache.cordova.inappbrowser/www/inappbrowser.js",
        "id": "org.apache.cordova.inappbrowser.inappbrowser",
        "clobbers": [
            "window.open"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.splashscreen/www/splashscreen.js",
        "id": "org.apache.cordova.splashscreen.SplashScreen",
        "clobbers": [
            "navigator.splashscreen"
        ]
    },
    {
        "file": "plugins/com.teamnemitoff.phonedialer/www/dialer.js",
        "id": "com.teamnemitoff.phonedialer.phonedialer",
        "merges": [
            "phonedialer"
        ]
    },
    {
        "file": "plugins/com.plugins.phonegap.SettingsPlugin/SettingsPlugin.js",
        "id": "com.plugins.phonegap.SettingsPlugin.SettingsPlugin",
        "clobbers": [
            "plugins.SettingsPlugin"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.dialogs/www/notification.js",
        "id": "org.apache.cordova.dialogs.notification",
        "merges": [
            "navigator.notification"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.vibration/www/vibration.js",
        "id": "org.apache.cordova.vibration.notification",
        "merges": [
            "navigator.notification",
            "navigator"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.network-information/www/network.js",
        "id": "org.apache.cordova.network-information.network",
        "clobbers": [
            "navigator.connection",
            "navigator.network.connection"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.network-information/www/Connection.js",
        "id": "org.apache.cordova.network-information.Connection",
        "clobbers": [
            "Connection"
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "org.apache.cordova.inappbrowser": "0.5.3",
    "org.apache.cordova.splashscreen": "0.3.4",
    "com.teamnemitoff.phonedialer": "0.3.0",
    "com.plugins.phonegap.SettingsPlugin": "0.1.0",
    "org.apache.cordova.geolocation": "0.3.10",
    "org.apache.cordova.dialogs": "0.2.10",
    "org.apache.cordova.vibration": "0.3.11",
    "org.apache.cordova.network-information": "0.2.13"
}
// BOTTOM OF METADATA
});