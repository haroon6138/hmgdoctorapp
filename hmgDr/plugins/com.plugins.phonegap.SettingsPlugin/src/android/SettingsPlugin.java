
package com.plugins.phonegap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.content.SharedPreferences;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;



public class SettingsPlugin extends CordovaPlugin {


	@Override
	public boolean execute(final String action, final JSONArray data, final CallbackContext callBackId) throws JSONException {
		
		JSONObject obj = new JSONObject();
		obj = data.getJSONObject(0);
		
		if (action.equals("changeLang")) {
			changeLanguage(obj.getString("lang"));
		}
		
		return true;
	}

	void changeLanguage (String lang){
		
		SharedPreferences settings = this.cordova.getActivity().getSharedPreferences("UserInfo", 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("appLanguage",lang);
		editor.commit();
	}

}
