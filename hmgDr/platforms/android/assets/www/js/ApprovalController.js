var ApprovalController = ApprovalController || {};
ApprovalController = (function () {
  var approvalPage = "#approval-page";
  var approvalList = [];
  var totalUnused=0;
  function init() {
     $(approvalPage).on("pagebeforeshow", function () {
      $("#approvalList").empty();
      if (approvalList.length > 0) {
        DisplayApproval();
      } else {
        getApproval();
      }
    });
  } // init()
   function getApproval(){
      var body = {
        PatientID:App.currentPatient.PatientID,
        //ProjectID: App.currentPatient.ProjectID,
        PatientTypeID: App.currentPatient.PatientType, 
        EXuldAPPNO: 0	  
      };
	    if(App.currentPatient.PatientStayType==2){  
		if(App.user.ProjectID==2){
		body.ProjectID=App.currentPatient.ProjectID;
		}else{
			body.ProjectID=0;
			}
		var request = new Req.RequestModel("approvalOut", body, retrieveApprovalSuccess, retrieveApprovalError);
      Req.sendRequest(request);
	  }else{
	  body.ProjectID=App.currentPatient.ProjectID;
	  		var request = new Req.RequestModel("approvalIn", body, retrieveApprovalSuccess, retrieveApprovalError);
      Req.sendRequest(request);
	  }

   }
  function retrieveApprovalSuccess(response) {
   if(App.currentPatient.PatientStayType==2){  
    approvalList = response.HIS_Approval_List;
	}else{
	approvalList = response.List_ApprovalMain_InPatient;
	}
	totalUnused=response.TotalUnUsedCount;
    $.mobile.changePage(approvalPage);
  } //retrieveApprovalSuccess
  function retrieveApprovalError(response) {
App.showErrorMsg(response.ErrorEndUserMessage);
  } //retrieveApprovalError
    function DisplayApproval() {
    var list = approvalList;
    var details = "";
    var innerHtml = "";
    var receiptOnDate;
	var expiryDate;
	var total=0;
    $("#approval-list-set").empty();
	$("#totalUnused").empty();
    var html = "";
	total=Loc.approvalPage.total+totalUnused;
	$("#totalUnused").append(total);
    for (var i = 0; i < list.length; i++) {
      var item = list[i];
     receiptOnDate = item.ReceiptOn;
	 expiryDate=item.ExpiryDate;
      var approvalStatusColor = "";
      if (item.Status == 9)
        approvalStatusColor = "green";
      else if (item.Status == 4 || item.Status == 10)
        approvalStatusColor = "red";
      else
        approvalStatusColor = "blue";
		
      if(receiptOnDate){
		receiptOnDate = new Date(parseInt(receiptOnDate.substring(6, receiptOnDate.length - 2)));
		receiptOnDate = dateFormat(receiptOnDate, "d/m/yyyy h:MM:ss TT")
	  }else{
		receiptOnDate = '';
	  }
      if(expiryDate){
		expiryDate = new Date(parseInt(expiryDate.substring(6, expiryDate.length - 2)));
		expiryDate= dateFormat(expiryDate, "d/m/yyyy");
	  }else{
		expiryDate = '';
	  }
      innerHtml = '<h3 class="collabsHeader">' + item.ClinicName + ' -<span class="ellipsis">' + item.DoctorName + '</span></h3>' + '<ul data-role="listview"  class="approval-list" data-theme="d" data-divider-theme="d" id="Approval-list">'
      innerHtml += '<li data-icon="false"  style="list-style: none; padding-bottom: 10px;" >' + '<div style="border:1px solid #C74B45;background-color:white;"  onclick="">' + '<table cellspacing="3px" style="width: 100%;" ><tbody>' + '<tr >' + '<td   >' + '<div class="approvalDetails" style="width: 75px;" >' + Loc.approvalPage.approvalNo + '</div>' + '</td>' + '<td style="">' + '<div class="approvalDetails">' + item.ApprovalNo + '</div>' + '</td>' + '</tr>' + '<tr >' + '<td style=""  >' + '<div class="approvalDetails">' + Loc.approvalPage.status + '</div>' + '</td>' + '<td style="" >' + '<div class="approvalDetails" style="color:' + approvalStatusColor + '">' + item.ApprovalStatusDescption + '</div>' + '</td>' + '</tr>' + '<tr >' + '<td   >' + '<div class="approvalDetails" style="width: 95px;" >' + Loc.approvalPage.usage + '</div>' + '</td>' + '<td style="" >' + '<div class="approvalDetails">' + item.UnUsedCount + '</div>' + '</td>' + '</tr>' +  '<tr >' + '<td style="" colSpan="3"; >' + '<div class="approvalDetails" >' + item.DoctorName + '</div>' + '</td>' + '</tr>' + '<tr >' + '<td style="" colSpan="2";>' + '<div class="approvalDetails" >' + item.CompanyName + '</div>' + '</td>' + '</tr>' + '<tr>' + '<td style=""  >' + '<div class="approvalDetails">' + Loc.approvalPage.receipton + '</div>' + '<div class="approvalDetails">' + receiptOnDate + '</div>' + '</td>' + '<td style=""  >' + '<div class="approvalDetails">' + Loc.approvalPage.expiry + '</div>' + '<div class="approvalDetails">' + expiryDate + '</div>' + '</td>' + '</tr>' + '</tbody></table>' + '</div>' + '</li>';
      innerHtml += '<li style="list-style: none; margin-left:0px;">' + '<table align="center" class="tableTheme approvalTable"  id="tableTheme" ><thead><tr><th>' + Loc.approvalPage.procedureName + '</th><th class="ResultValue">' + Loc.approvalPage.procedurestatus + '</th><th class="ResultValue">' + Loc.approvalPage.usageStatus + '</th></tr></thead><tbody >'
      if (item.ApporvalDetails.length != 0) {
        for (var v = 0; v < item.ApporvalDetails.length; v++) {
          innerHtml += '<tr class="">' + '<td class="">' + item.ApporvalDetails[v].ProcedureName + '</td>' + '<td class="">' + item.ApporvalDetails[v].Status + '</td>' +'<td class="">' + item.ApporvalDetails[v].IsInvoicedDesc + '</td>' + '</tr>';
        }
        innerHtml += '</tbody></table></li></ul>';
      }
      var html = $('<div data-role="collapsible" data-expanded-icon="arrowUp" data-collapsed-icon="arrowDown" > </div>');
      html.append(innerHtml);
      $('#approval-list-set').append(html);
      $('#approval-list-set').collapsibleset('refresh');
    }
  } //DisplayApproval
  return {
    init: init,
	getApproval:getApproval
  }
})();