//
//  settingsPlugin.m
//  HMG
//
//  Created by Admin on 7/24/13.
//
//

#import "SettingsPlugin.h"

@implementation SettingsPlugin



- (void)changeLang:(CDVInvokedUrlCommand*)command{
    
    NSDictionary* dicString = [command argumentAtIndex:0];
    NSString *appLang= [dicString objectForKey:@"lang"];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setValue:appLang forKey:@"appLanguage"];
}
@end
