{
    "homePage": {
        "schedule": ["My Schedule", "الجدول"],
		"search": ["Patient Search", "البحث عن مريض"],
		"reply": ["Doctor Reply", "رد الطبيب"],
		"bloodBank": ["Blood Bank", "بنك الدم"],
		"medicine": ["Search Medicine", "البحث عن دواء"],
		"discharged": ["Discharged Patients", "المريض المعافى"],
		"inpatient": ["Inpatients", "المرضى المنومين"],
		"tomorrowpatient":["Tomorrow Patients","مرضى الغد"],
		"outpatient":["Outpatient","المريض الخارجي"],
		"referral":["Referral","المحالين إلي"],
		"referred":["Referred","المحولين مني"],
		"refdischarged":["Referral Discharged","المُحالين المتعافين"],
		"refrreddischarged":["Referred Discharged","المُحولون المتعافين"],
		"header":["Home","الرئيسية"]
    },
	"searchPatientPage": {
	    "header": ["Search Patient", "البحث عن المريض"],
		"OutPatienPeriodChoice1": ["Today", "اليوم"],
		"OutPatienPeriodChoice2": ["Tomorrow", "غدًا"],
		"OutPatienPeriodChoice3": ["Next Week", "الأسبوع القادم"],
		"OutPatienPeriodChoice4": ["Next Month", "الشهر القادم"],
		"results": ["Patients", "المرضى"],
        "type": ["Paitent Type", "نوع المريض"],
		"fname": ["First Name", "الاسم الأول"],
		"mname": ["Middle Name", "الاسم الأوسط"],
		"lname": ["Last Name", "اسم العائلة"],
		"mnumber": ["Mobile Number", "رقم الجوال"],
		"pID": ["Patient ID#", "رقم الهوية"],
		"pFile": ["Patient File#", "رقم الملف"],
		"from": ["From", "من"],
		"to": ["To", "إلى"],
		"pLocation": ["Patient", "المريض"],
		"pLocation1": ["In Saudi Arabia", "داخل المملكة"],
		"pLocation2": ["Outside Saudi Arabia", "خارج المملكة"],
		"patientType1": ["Outpatient", "المريض الخارجي"],
		"patientType2": ["Inpatient", "المريض المنوم"],
		"patientType3": ["Discharged", "المريض المعافى"],
		"patientType4": ["Referral", "المريض المحول إلي"],
		"patientType5": ["Referred", "المريض المحول مني"],
		"patientType6": ["Referral Discharged", "المريض المحال المعافى"],
		"patientType7": ["Referred Discharged", "المريض المحول المعافى"],
		"patientType8": ["Tomorrow Paitents", "مريض الغد"],
		"arrivedPatient": ["Only Arrived Patients", "المريض الذي حضر للموعد"],
		"name1": ["First Name", "الاسم الاول بالإنجليزي"],
        "name2": ["Middle Name", "الاسم الاوسط بالإنجليزي"],
        "name3": ["Last Name", "اسم العائلة بالإنجليزي"]	,
		"dateFormat":["yyyy-mm-dd","yyyy-mm-dd"],
		"search":["Search","بحث"],
		"findPatient":["Find a patient","ابحث عن مريض"]
    },
	"settingsPage": {
        "header": ["Settings", "الإعدادات"],
        "ar": ["عربي", "عربي"],
        "en": ["English", "English"],
        "language": ["Language", "اللغة"]
    },
		"vitalSignsPage": {
		"header": ["Vital Signs", "العلامات الحيوية"],
        "temperature": ["Temperature", "درجة الحرارة"],
        "respiration": ["Respiration", "التنفس"],
        "bloodPressure": ["Blood Pressure", "ضغط الدم"],
        "Oxyg": ["Oxygenation", "الأوكسجين"],
		"painSc": ["Pain Scale", "مقياس الألم"],
		"pulse": ["Pulse", "النبض"],
		"BDM": ["Body Measurements","قياسات الجسم"],
		"weight": ["Weight(kg):","الوزن (كحم):"],
		"height": ["Height (cm):","الطول (سم):"],
		"bmi": ["Body Mass Index:","مؤشر كتلة الجسم"],
		"HeadCircumCm": ["Head Circum (Cm):","محيط الرأس (سم):"],
		"IBWLbs": ["Ideal Body Weight (Lbs):","الوزن المثالي للجسم (رطل):"],
		"LBWLbs": ["Lean Body Weight(Lbs):"," وزن الجسم الهزيل(رطل):"],
		"gcs": ["G.C.S:","G.C.S:"],
		"triageCatg": ["Triage Manchestor Category:","Triage Manchestor Category:"],
		"temperatureC": ["Temperature (°C):","درجة الحرارة المئوية:"],
		"temperatureMethod": ["Temperature Method:","طريقة درجة الحرارة:"],
		"pulseMsr": ["Pulse (beats/minute):","النبض (نبضة / دقيقة):"],
		"rhythm": ["Rhythm:","التواتر:"],
		"resp": ["RESP (beats/minute):","التنفس (نبضة / دقيقة):"],
		"pattern": ["Pattern or Respiration:","النمط أو التنفس:"],
		"bdPressureLower": ["Blood Pressure Lower:","ضغط الدم السفلى:"],
		"bdPressureHigher": ["Blood Pressure Higher:","ضغط الدم العالي:"],
		"patientPos": ["Patient Position:","وضع المريض:"],
		"cuffLoc": ["Cuff Location:","مكان الكفة:"],
		"cuffSize": ["Cuff Size:","مقاس الكفة:"],
		"oxso": ["SAO2 (%):","SAO2 (%):"],
		"oxfi": ["FIO2 (%):","FIO2 (%):"],
		"painScale": ["Pain Scale:","مقياس الألم:"],
		"PainLoc": ["Location:","المكان:"],
		"PainCh": ["Character:","الصفة:"],
		"painDur": ["Duration:","المدة:"],
		"painAssm": ["Pain Assessment Done?","تقييم الألم حصل:"],
		"painfrq": ["Frequency:","التكرار:"],
		"PainScoreDesc":["Description:","الوصف:"]
    },
    "presPage": {
        "header": ["Prescriptions", "الوصفات الطبية"]
    },
	"schedulePage": {
        "header": ["My Schedule", "جدولي"]
    },
	   "LabResultsPage": {
        "header": ["Lab Orders", "الفحوصات الطبية"],
        "detailsHeader": ["Lab Results", "نتائج الفحوصات"],
        "invoiceTitle": ["Invoice No: ", "رقم الفاتورة:"]
    },
	   "searchMedicinePage": {
        "header": ["Search Medicine", "البحث عن دواء"],
		"header2": ["Pharmacies","الصيدليات"]
    },
	 "mapPage": {
        "header": ["Pharmacies", "الصيدليات"],
        "call": ["Call", "اتصل"],
        "directions": ["Directions", "اتجهاهات"],
		"directionsClear": ["Release", "المواقع"]
    },
	"loginPage": {
        "header": ["login", "تسجيل الدخول"],
        "id": ["ID", "الهوية"],
        "password": ["Password", "الرقم السري"],
		"footer":["All rights reserved", "جميع الحقوق محفوظة"],
		"continue":["Continue","متابعة"],
		"lang":["عربي","English"]
    },
	    "validatePage": {
        "header": ["Validation", "تأكيد"],
        "msg": ["Validation code has been sent to you by SMS, Please enter the code below to login", "تم ارسال رسالة نصية برقم التفعيل, برجاء ادخال الرقم لاستكمال عملية الدخول"],
		"validatebtn": ["Submit", "متابعة"],
		"msg1": ["The validation code expires within:", "الرقم السري ينتهي خلال:"]
	},
	"radiologyPage":{
	"header": ["Radiology", "الأشعة"],
	"headerDetails": ["Radiology Report", "تقرير الأشعة"],
	"emailRadReport":["Email Radiology Report","ارسل نسخة من تقرير الأشعة"],
	"seeImage": ["See Image","شاهد صورة الأشعة"]
	},
	"drProfile":{
	"header": ["My Profile", "ملفي الشخصي"],
	"logout":["Logout","خروج"]
	},
	"drReply":{
	"header": ["Requests", "الطلبات"],
	"replyh":["Reply to Request","الرد على الطلب"],
	"response":["Response","الرد"]
	},
	"approvalPage":{
	"header": ["Approval", "موافقات التأمين"]
	},
	"ReferralPage":{
	"header": ["Refer Patient", "تحويل المريض"],
	"ReplyBefore": ["Replay Before: ", "اجب قبل: "],
	"clinicSelect": ["Clinic", "العيادة"],
	"doctorSelect": ["Doctor", "الدكتور"],
	"ext":["Ext#","التحويلة"],
	"urgent":["Urgent","عاجل"],
	"vurgent":["Very Urgent","عاجل جدا"],
	"routine":["Routine","روتين"],
	"priority":["Priority","الأولوية"],
	"frequency":["Referral Frequency","نوع التحويل"],
	"clinicalDetails":["Clinical Details and Remarks","تفاصيل السريرية وملاحظات"]
	},
	"navPanel":{
	"schedule":["My Schedule","جدولي"],
	"settings":["Settings","الإعدادات"]
	},
	"patientProfile":{
	"header": ["Patient Profile", "ملف المريض"]
	},
	"bloodBankPage":{
	"header": ["Blood Bank", "بنك الدم"],
	"chooseType":["Choose Blood Group","اختر فصيلة الدم"]
	},
	 "noNetpage": {
        "header": ["No Network", "لايوجد شبكة"],
		"noInternet": [" Cannot Connect to the Internet", "لايمكن الاتصال بالإنترنت"],
		"noInternetDetails": ["You must connect to a wifi network or a valid internet connection.", " يجب عليك الاتصال بشبكة الواي فاي أو التأكد من وجود اتصال بالإنترنت."]
 
    }
}
