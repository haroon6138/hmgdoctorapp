﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WPCordovaClassLib.Cordova;
using WPCordovaClassLib.Cordova.Commands;
using WPCordovaClassLib.Cordova.JSON;
using System.Runtime.Serialization;
using System.IO.IsolatedStorage;

namespace Cordova.Extension.Commands
{
    public class SettingsPlugin : BaseCommand
    {

        #region SettingsPlugin Options

        /// <summary>
        /// Represents LocalNotification options
        /// </summary>
        [DataContract]
        public class SettingsOptions
        {
            /// <summary>
            /// Initial value for time or date
            /// </summary>
            [DataMember(IsRequired = false, Name = "lang")]
            public String Lang { get; set; }

            /// <summary>
            /// Creates options object with default parameters
            /// </summary>
            public SettingsOptions()
            {
                this.SetDefaultValues(new StreamingContext());
            }

            /// <summary>
            /// Initializes default values for class fields.
            /// Implemented in separate method because default constructor is not invoked during deserialization.
            /// </summary>
            /// <param name="context"></param>
            [OnDeserializing()]
            public void SetDefaultValues(StreamingContext context)
            {
                this.Lang = "ar";
            }

        }
        #endregion

        public void changeLang(string jsonArgs)
        {

            try
            {
                var options = JsonHelper.Deserialize<string[]>(jsonArgs);
                var langOption = options[0];
              SettingsOptions settingsOptions = String.IsNullOrEmpty(langOption) ? new SettingsOptions() :
              JsonHelper.Deserialize<SettingsOptions>(langOption);

                if(WriteSettings("lang", settingsOptions.Lang))
                    DispatchCommandResult(new PluginResult(PluginResult.Status.OK, "Success"));
                else
                    DispatchCommandResult(new PluginResult(PluginResult.Status.ERROR));
            }
            catch (Exception e)
            {
                DispatchCommandResult(new PluginResult(PluginResult.Status.ERROR, e.Message));
            }
        }

        Boolean WriteSettings(string key, string value)
        {
            try
            {
                if (IsolatedStorageSettings.ApplicationSettings.Contains(key))
                {
                    IsolatedStorageSettings.ApplicationSettings[key] = value;
                }
                else
                {
                    IsolatedStorageSettings.ApplicationSettings.Add(key, value);
                }

                IsolatedStorageSettings.ApplicationSettings.Save();

                return true;
            }
            catch
            {
                return false;
            }
        }

    }
}
