var LabResultsController = LabResultsController || {};
LabResultsController = (function () {
  var labAppoPage = "#LabAppo-page";
  var labPage = "#LabResults-page";
  var labAppoList = [];
  var labResultList = [];
  var labSpecialResultList = [];
  var isSpecialResultSet = 0;
  var hasGenResults = false;
  var hasSPCResults = false;
  var missingResults = false;
  var labReportData = "";
  var userProfile;
  var imgsArray = ["img/dr_m.png", "img/dr_f.png"];
  var labIndex = 0;

  function init() {
    $(labAppoPage).on("pagebeforeshow", function () {
      $("#labAppoList").empty();
      if (labAppoList.length > 0) {
        DisplayLabAppoList();
      } else {
        DisplayNoRecordMessage();
      }
    });
    $(labPage).on("pagebeforeshow", function () {
      $("#lab-list-set").empty();
      var body = {
        ProjectID: tempLabAppo.ProjectID,
        SetupID: tempLabAppo.SetupID,
        OrderNo: tempLabAppo.OrderNo,
        InvoiceNo: tempLabAppo.InvoiceNo,
        PatientID: App.currentPatient.patientID,
		PatientTypeID:App.currentPatient.PatientType
      };
      var request = new Req.RequestModel("labResList", body, retrieveLabResultSuccess, retrieveLabResultError);
      Req.sendRequest(request);
      // var body = {
        // ProjectID: tempLabAppo.ProjectID,
        // SetupID: tempLabAppo.SetupID,
        // InvoiceNo: tempLabAppo.InvoiceNo,
        // PatientID: App.currentPatient.patientID,
		// PatientTypeID:App.currentPatient.PatientType
      // };
      // var request = new Req.RequestModel("labResListSpecial", body, retrieveLabSpecialResultSuccess, retrieveLabSpecialResultError);
      // Req.sendRequest(request);
    });
  } // init()
  //*************** Lab Appo Screen func ***********************
    function setLabAppointmentList(obj){
  labAppoList=obj;
   $.mobile.changePage(labAppoPage);
  }
  //*************** Lab Results Screen func ***********************
  function DisplayLabDetails(index) {
    labIndex = index;
    tempLabAppo = labAppoList[index];
    $.mobile.changePage("#LabResults-page");
  }

  function retrieveLabResultSuccess(response) {
  if(response.LabResultType==3){
	labResultList = response.List_GetLabNormal;
	labSpecialResultList = response.List_GetLabSpecial;
	hasGenResults = true;
	hasSPCResults = true;
	DisplayLabResList();
	DisplayLabSpecialResList();
  }else if(response.LabResultType==1){
  labResultList = response.List_GetLabNormal;
  hasGenResults = true;
  hasSPCResults=false;
  DisplayLabResList();
  }else if(response.LabResultType==2){
  labSpecialResultList = response.List_GetLabSpecial;
  hasGenResults = false;
  hasSPCResults=true;
  DisplayLabSpecialResList();
  }

    if (hasGenResults == true || hasSPCResults == true) {
      $("#lab-list-set").show();
      $('#PendingDataDiv').hide();
      $('#NoDataDiv').hide();
    }
    if (hasGenResults == false && hasSPCResults == false) {
      DisplayPendingReslutsMessage();
      $('#PendingDataDiv').show();
    }
  }

  function retrieveLabResultError(response) {
    App.showErrorMsg(response.ErrorEndUserMessage);
  }

  function retrieveLabSpecialResultSuccess(response) {
    labSpecialResultList = response.ListPLSR;
    if (labSpecialResultList.length > 0) {
	if(labSpecialResultList[0].ResultDataHTML){
      labSpecialResultList = response.ListPLSR;
      hasSPCResults = true;
      DisplayLabSpecialResList();
    } else {
      hasSPCResults = false;
      $('#PendingDataDiv').hide();
    }
	}else {
      hasSPCResults = false;
      $('#PendingDataDiv').hide();
	}
    if (hasGenResults == true || hasSPCResults == true) {
      $("#lab-list-set").show();
      $('#PendingDataDiv').hide();
      $('#NoDataDiv').hide();
    }
    if (hasGenResults == false && hasSPCResults == false) {
      DisplayPendingReslutsMessage();
      $('#PendingDataDiv').show();
    }
  }

  function retrieveLabSpecialResultError(response) {
    App.showErrorMsg(response.ErrorEndUserMessage);
  }

  function DisplayLabAppoList() {
    var list = labAppoList;
    var dateAppo;
    $("#labAppoList").empty();
    var html = "";
    for (var i = 0; i < list.length; i++) {
      var item = list[i];
      dateAppo = item.OrderDate;
      dateAppo = App.ConvertDate(dateAppo);
  //html += '<li data-icon="" style="background: transparent;padding-top: 0px;padding-bottom: 0px;border-bottom: 0px;" >' + '<div width="320px" style="border-bottom:1px solid #C74B45;background-color:white;"  onclick="LabResultsController.DisplayLabDetails(' + i + ')">' + '<table width="320px" border="0" cellspacing="3px"> <thead class="ui-widget-header"> </thead><tbody class="ui-widget-content">' + '<tr >' + '<td  width="80px" rowspan="3">' + '<div>' + '<img width="75px" height="110px" src="' + item.DoctorImageURL + '" onError="this.onerror=null;this.src=\'' + imgsArray[0] + '\';" align="middle" />' + '</div>' + '</td>' + '<td style="" colspan="3">' + '<div>' + '<div class="appoTitle" style="font-size:14px;">' + item.ClinicDescription+ '</div>' + '<div class="appoDrName">' + item.DoctorName + '</div>' + '</div>' + '<hr class="splitter loginFormSplitter">' + '</td>' + '<td   rowspan="4" align="center" class="arrow-icon">' + '</td>' + '</tr>' + '<tr>' + '<td style="" >' + '<img class="app-cal-img"  src="img/cal.png">' + '<div class="appoDetails">' + dateFormat(dateAppo, "d/m/yyyy") + '</div>' + '</td>' + '</tr>' + '<tr>' + '<td style=""  >' + '<img class="app-cal-img"  src="img/invoice.png">' + '<div class="appoDetails">' + Loc.labPage.invoiceTitle+ item.InvoiceNo  + '</div>' + '</td>' + '</tr>' + '</tbody></table>' + '</div>' + '</li>';
    html += '<li>' + '<div "style="border:0px; background-color:white;"  onclick="LabResultsController.DisplayLabDetails(' + i + ')">' + '<table width="100%" border="0" cellspacing="3px"> <thead class="ui-widget-header"> </thead><tbody class="ui-widget-content">' + '<tr class="presAppoDetails" >' + '<td style="width:7%;vertical-align: top;" rowspan="3">' + '<div>' + '<img style="border:1px solid;margin-top: 3px;" width="70px" height="80px" src="' + item.DoctorImageURL + '" onError="this.onerror=null;this.src=\'' + imgsArray[0] + '\';" align="middle" />' + '</div>' + '</td>' + '<td style="" colspan="2">' + '<div>' + '<div class="appoDrName">' + item.DoctorName + '</div>' + '<div style="font-size:12px; color:#ed1b2c;">' + item.ClinicName + '</div>' + '<div>'+Loc.labPage.invoiceTitle+ item.InvoiceNo+'</div>'+'</div>' + '</td>' + '<td  rowspan="3" align="center" class="arrow-icon">' + '</td>' + '</tr>' + '<tr class="presAppoDetails" >' + '<td>' + '<img class="presSmallImage" src="img/calendar.png" width="30px" height="30px">'+ "  " + dateAppo + '</td>'  +'</tr> <tr class="presAppoDetails"> <td style="vertical-align: top;">' + '<img class="presSmallImage" style="top:-7px;" src="img/pin.png" width="30px" height="30px">' + "  " + item.ProjectName + '</td>' + '</tr>' + '</tbody></table>' + '</div>' + '</li>';
  
   }
    $("#labAppoList").append(html).listview('refresh');
  }

  function DisplayLabResList() {
    var list = labResultList;
    var html = "";
    var innerHtml = "";
    var lab = 0;
    innerHtml = '<h3 class="collabsHeader">' + Loc.labPage.GeneralResultsTitle + '</h3>' + '<ul data-role="listview"  style="margin-left: 0px;margin-right: 0px;" class="lab-list" data-theme="d" data-divider-theme="d" id="list-1">' + '<li style="list-style: none; margin-left:0px;padding: 0px;">' + '<table cellpadding="3" cellspacing="0" align="center" class="tableTheme2" id="tableTheme" ><thead><tr><th>Test Description</th><th class="ResultValue">Result Value</th><th>Reference Range</th></tr></thead><tbody >'
    for (var i = 0; i < list.length; i++) {
      lab = list[i];
      innerHtml += '<tr id="list-' + i + '" class="results">' + '<td class="rowDescription" style="text-align: left !important;">' + lab.Description + '</td>' + '<td class="rowResultValue">' + lab.ResultValue + '</td>' + '<td class="rowReferanceRange">' + lab.ReferenceRange + '</td>' + '</tr>';
      var listId = "#list-" + i;
    }
    innerHtml += '</tbody></table></li></ul>';
    if (i == 0) {
      var html = $('<div data-role="collapsible" data-expanded-icon="close" data-collapsed-icon="open" > </div>');
    } else {
      var html = $('<div data-role="collapsible" data-expanded-icon="close" data-collapsed-icon="open" > </div>');
    }
    $('#PendingDataDiv').hide();
    $('#NoDataDiv').hide();
    html.append(innerHtml);
    $('#lab-list-set').append(html);
    $("#list-1").listview().listview('refresh');
    html = "";
    $('#lab-list-set').collapsibleset('refresh');
    labReportData = $('#lab-list-set table').html();
  }

  function DisplayLabSpecialResList() {
    var list = labSpecialResultList;
    var html = "";
    var innerHtml = "";
    var lab = 0;
    var itemCheck = 999;
    var resultValue = "";
    itemCheck = list[0];
    if (itemCheck.ResultDataHTML == null) {
      missingResults = true;
    } else {
      innerHtml = '<h3 class="collabsHeader">' + Loc.labPage.SpecialResultsTitle + '</h3>' + '<ul data-role="listview"  class="lab-list" data-theme="d" data-divider-theme="d" id="LabSP-Result-list"><li style="list-style: none;padding: 0px;">';
      innerHtml += '<table style="width: 100%;white-space: normal;" class="result-list"><thead></thead><tbody>';
      for (var i = 0; i < list.length; i++) {
        lab = list[i];
        resultValue = lab.ResultDataHTML.replace(/\n/g, "<br>");
        resultValue = resultValue.replace(/\t/g, "");
        innerHtml += '<tr id="list-' + i + '" class="results"><td class="rowDescription">' + resultValue + '</td></tr>';
        var listId = "#list-" + i;
      }
      innerHtml += '</tbody></table></li></ul>';
      if (i == 0) {
        var html = $('<div data-role="collapsible" data-expanded-icon="close" data-collapsed-icon="open">  </div>');
      } else {
        var html = $('<div data-role="collapsible" data-expanded-icon="close" data-collapsed-icon="open"> </div>');
      }
      $('#PendingDataDiv').hide();
      $('#NoDataDiv').hide();
      html.append(innerHtml);
      $('#lab-list-set').append(html);
      $("#LabSP-Result-list").listview().listview('refresh');
      html = "";
    }
    $('#lab-list-set').collapsibleset('refresh');
    if (hasGenResults == false && hasSPCResults == false) {
      //$('#PendingDataDiv').hide();
    } else {
      labReportData = $('#lab-list-set table').html();
    }
  }

  function DisplayNoRecordMessage() {
    $("#labAppoList").empty();
    var html = "";
    var requestType = 0;
    var listId = "#list-1";
    html = '<div id="NoDataDiv" width="320px" style="padding: 10px;border:1px solid #c74b45;background-color:white;white-space: normal;">'
     if (App.currentPatient.PatientStayType == 2) {
      html += '<div>' + Loc.msg.NoDataYet + '</div>';
    } else {
      html += '<div>' + Loc.msg.noResults + '</div>';
    }
    html += '</div>';
    $('#labAppoList').append(html).listview('refresh');
  }

  function DisplayPendingReslutsMessage() {
    var html = "";
	$('#lab-list-set').empty();
    var requestType = 0;
    var listId = "#list-1";
    html = '<div id="PendingDataDiv" width="320px" style="padding: 10px;border:1px solid #c74b45;background-color:white;white-space: normal;">'
    html += '<div>' + Loc.labPage.pendingDataOrNoRec + '</div>';
    html += '</div>';
    $('#lab-list-set').append(html);
  }

  return {
    init: init,
    DisplayLabDetails: DisplayLabDetails,
    setLabAppointmentList: setLabAppointmentList
  }
})();