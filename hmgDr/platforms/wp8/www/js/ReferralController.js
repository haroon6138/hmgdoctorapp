var ReferralController = ReferralController || {};
var referralPage ="#referral-page";
var referListPage="#referList-page";
var freqCount=0;
var clinicCount=0;
var freqList=[];
var referralList=[];
var clinicList=[];
var doctorList=[];
ReferralController = (function () {
  function init() {
   $(referralPage).on("pagebeforeshow", function () {
   retrieveClinicList();
   CalculateReferBeforeDate(); //Shaden Edition 
   $('textarea#detailsTextArea').val("");
   freqCount=0;
   clinicCount=0;
	 loadFrequencyList();
});
   $(referListPage).on("pagebeforeshow", function () {

});
$("#clinic-selectList").on("change focus", function () {
 var body = {
   ProjectID:App.user.ProjectID,
   ClinicID:$("#clinic-selectList").val()
      };
      var request = new Req.RequestModel("doctorClinic", body, retrieveDoctorListSuccess, retrieveDoctorListError);
      Req.sendRequest(request);
});
  } // init()
  function retrieveDoctorListSuccess(response){
  $("#doctor-selectList").empty();
 doctorList= response.List_Doctors_All;
  var html="";
  if(doctorList){
    html += '<option value="0">' + Loc.referralPage.chooseDoctor + '</option>';
  
for(var i=0;i<doctorList.length;i++){
html+='<option value="'+doctorList[i].DoctorID+'">'+doctorList[i].DoctorName+'</option>';
}
  $("#doctor-selectList").append(html).selectmenu("refresh");
}
  }
   function retrieveDoctorListError(response){
  }
  function retrieveClinicList(){
   var body = {
   ProjectID:App.user.ProjectID
      };
      var request = new Req.RequestModel("clinicProject", body, retrieveClinicListSuccess, retrieveClinicListError);
      Req.sendRequest(request);
  }
  function retrieveClinicListSuccess(response){
   $("#clinic-selectList").empty();
   clinicList= response.List_Clinic_All;
     var html=""; 
	 if(clinicList){
    html += '<option value="0">' + Loc.referralPage.chooseClinic + '</option>';
for(var i=0;i<clinicList.length;i++){
html+='<option value="'+clinicList[i].ClinicID+'">'+clinicList[i].ClinicDescription+'</option>';
}
  $("#clinic-selectList").append(html).selectmenu("refresh");
}
  }
    function retrieveClinicListError(response){
	if(clinicCount<3){
retrieveClinicList();
}else{
 App.showErrorMsg(response.ErrorEndUserMessage);
}
clinicCount++;
  }
  function goToReferralPage(){
$.mobile.changePage("#referral-page");
}
function loadFrequencyList(){
 var body = {
      };
      var request = new Req.RequestModel("frequencyList", body, loadFrequencyListSuccess, loadFrequencyListError);
      Req.sendRequest(request);
}
function loadFrequencyListSuccess(response){
 $("#frequency-select").empty();
freqList= response.list_STPReferralFrequency;
var html="";
if(freqList){
for (var i = 0; i < freqList.length; i++) {
        var item = freqList[i];
        html += "<option value=" + item.ParameterCode + ">" + item.Description + "</option>";
      }
      $("#frequency-select").append(html).selectmenu("refresh");
	  }
}
function loadFrequencyListError(response){
if(freqCount<3){
loadFrequencyList();
}else{
 App.showErrorMsg(response.ErrorEndUserMessage);
}
freqCount++;
}
function referToDoctor(){
var priority= $("input[name*=priorityChoice]:checked").val();
  //var requestText=$('textarea#detailsTextArea').val();
  var isValidFields=false;
  isValidFields=validateReferralFields();
  if(isValidFields){
 var body = {
 ProjectID :App.user.ProjectID,
AdmissionNo:App.currentPatient.AdmissionNo,
RoomID:App.currentPatient.RoomID,
ReferralClinic:$('#clinic-selectList').val(),
ReferralDoctor:$('#doctor-selectList').val(),
CreatedBy :App.user.DoctorID,
EditedBy :App.user.DoctorID,
PatientID :App.currentPatient.PatientID,
PatientTypeID :App.currentPatient.PatientTypeID,
ReferringClinic :App.user.ClinicID,
ReferringDoctor :App.user.DoctorID,
ReferringDoctorRemarks :$('textarea#detailsTextArea').val(),
Priority :priority,
Frequency :$("#frequency-select").val(),
Extension:$("#extInput").val()
      };
      var request = new Req.RequestModel("referToDoctor", body, referToDoctorSuccess, referToDoctorError);
      Req.sendRequest(request);
}else{
App.showErrorMsg(Loc.referralPage.errorMsg);
  }
  }
  function validateReferralFields(){
  var requestText=$('textarea#detailsTextArea').val();
  var isValidClinic=false;
  var isValidDoctor=false;
  var isValidText=false;
  var isValidExt=false;
  var isValidFrequency=false;
  if(requestText){
  isValidText=true;
  }else{
  isValidText=false;
  }
  if($('#clinic-selectList').val() && $('#clinic-selectList').val()!='0'){
  isValidClinic=true;
  }else{
  isValidClinic=false;
  }
if($('#doctor-selectList').val() && $('#doctor-selectList').val()!='0'){
isValidDoctor=true;
}else{
isValidDoctor=false;
}
if($("#extInput").val()){
isValidExt=true;
}else{
isValidExt=false;
}
if($("#frequency-select").val()){
isValidFrequency=true;
}else{
isValidFrequency=false;
}
if(isValidClinic==true && isValidDoctor==true && isValidText==true && isValidExt==true &&isValidFrequency==true){
return true;
}else{
return false;
}
  }
function referToDoctorSuccess(response){
$.mobile.changePage('#patientProfile-page');
}
function referToDoctorError(response){
App.showErrorMsg(response.ErrorEndUserMessage);
}
function displayReferrals(refList,isReferredList){
if(refList){
referralList=refList;
  var list = refList;
    $("#refer-list-set").empty();
     var html = "";
     var innerHtml = "";
     var item;
   for (var i = 0; i < list.length; i++) {
      item = list[i];
	  var patientName=item.FirstName+" "+item.LastName
innerHtml = '<h3>' + patientName+'<span  id="ReferalCheckSpan-'+i+'" style="float:right;display:none;" ><img src="img/checkDone.png" class="ReferalCheckImg" id="ReferalCheck'+i+'" /></span></h3>' + '<ul data-role="listview" class="referListDetails" style="margin:0px;list-style: none;" id="referListDetails-'+i+'">' +
                  '<li>' +'<table width="100%" cellspacing="0" class="prescriptionListTableDetail">'
				  +'<tr><td colspan="2"><a href="#" class="customProfileBtn ui-btn ui-icon-userProfile ui-btn-icon-right ui-btn-inline ui-corner-all ui-mini" data-role="button" role="button" onclick="ReferralController.goToPatientProfilePage('+i+');"></a></td></tr>'
				   	 // +'<tr><td colspan="2"><input data-theme="custom-button-reply" type="button" data-icon="reply"  id="addReplyButton-' + (i + 1)+'" onclick="DoctorReplyController.goToReplyPage('+i+');" value="'+Loc.doctorReplyPage.reply+'"></input></td></tr>';
						 +'<tr><td class="redColorForText">'+ Loc.referralPage.file +'</td><td>'+item.PatientID +'</td></tr>'
						 if(isReferredList){
						  innerHtml+='<tr><td class="redColorForText">'+ Loc.referralPage.referralDoctor +'</td><td>'+ item.ReferralDoctorName +'</td></tr>'
						  +'<tr><td class="redColorForText">'+ Loc.referralPage.referralClinic +'</td><td>'+item.ReferralClinicDescription +'</td></tr>'
						 }else{
						  innerHtml+='<tr><td class="redColorForText">'+ Loc.referralPage.referDoctor +'</td><td>'+ item.ReferringDoctorName +'</td></tr>'
						  +'<tr><td class="redColorForText">'+ Loc.referralPage.referClinic +'</td><td>'+item.ReferringClinicDescription +'</td></tr>'
						 }
						
						   innerHtml+='<tr><td class="redColorForText">'+ Loc.referralPage.frequency +'</td><td>'+item.FrequencyDescription +'</td></tr>'
						    +'<tr><td class="redColorForText">'+ Loc.referralPage.priority +'</td><td>'+item.PriorityDescription +'</td></tr>'
							+'<tr><td class="redColorForText">'+ Loc.referralPage.MAXResponseTime +'</td><td>'+App.ConvertDate(item.MAXResponseTime) +'</td></tr>'
							+'<tr><td colspan="2" class="redColorForText">'+ Loc.referralPage.clinicRemarks +'</td></tr>'
							+'<tr><td colspan="2">'+item.ReferringDoctorRemarks +'</td></tr>'
			if(item.ReferralStatus==2){
			innerHtml+='<tr><td colspan="2" class="redColorForText">'+ Loc.referralPage.referAnswer +'</td></tr>'
	        +'<tr><td colspan="2">'+item.ReferredDoctorRemarks +'</td></tr>';
			}else{
			var remark=item.ReferredDoctorRemarks;
			if(remark){
			remark=item.ReferredDoctorRemarks;
			}else{
			remark="";
			}
			
			 if(isReferredList){
			 	innerHtml+='<tr><td colspan="2" class="redColorForText">'+ Loc.referralPage.referAnswer +'</td></tr>'
	        +'<tr><td colspan="2">'+item.ReferredDoctorRemarks +'</td></tr>';
			if(item.ReferredDoctorRemarks){
			innerHtml+='<tr><td colspan="2"><a href="#" data-theme="custom-button-all" class="customReferReplyBtn" data-role="button" role="button" onclick="ReferralController.VerifyReferralDocRemarks('+i+');">'+Loc.referralPage.verifyButton+'</a></td></tr>';
			}
			}else{
			innerHtml+= '<tr><td colspan="2"><div data-role="fieldcontain">'
			+' <label id="responseLabel-'+i+'" for="ReferredDocRemark-'+i+'" class="redColorForText">'+Loc.referralPage.referAnswer+'</label>'
			+'<textarea name="ReferredDocRemark-'+i+'"  style="height: 51px" id="ReferredDocRemark-'+i+'">'+remark+'</textarea></div></td></tr>'
			+'<tr><td colspan="2"><a href="#" data-theme="custom-button-all" class="customReferReplyBtn" data-role="button" role="button" onclick="ReferralController.replyToReferredDoctor('+i+');">'+Loc.referralPage.replyButton+'</a></td></tr>'
			}
			}
						 //+replyButton
             innerHtml+= '</table>' + '</li>' + '</ul>';
var html = $('<div data-role="collapsible" ></div>');
       var listId = "#referListDetails-"+i ;
      html.append(innerHtml);
      $('#refer-list-set').append(html);
	  //$("#addReplyButton-"+(i+1)).button().button('refresh');
      $(listId).listview();
      $(listId).listview('refresh', true);
	   var referalCheckSpan="#ReferalCheckSpan-"+i ;
	   if(item.ReferralStatus==2){
			$(referalCheckSpan).show();
	  }else{
	  var remarkImage=item.ReferredDoctorRemarks;
			if(remarkImage){
			$(referalCheckSpan+'>img').attr('src','img/checkReady.png');
			$(referalCheckSpan).show();
			}else{
	  			$(referalCheckSpan).hide();
				}
	  }
      html = "";
}

	$('#refer-list-set').collapsibleset().trigger('create');
	}
	$.mobile.changePage(referListPage);
}

function goToPatientProfilePage (index){
App.setBackPage("#profileBackKey","#referList-page");
var obj=referralList[index];
obj.isDischarged=false;
obj.PatientStayType=1;
  App.currentPatient=obj;
  obj.AdmissionNo=parseInt(obj.AdmissionNo);
  PatientProfileController.setPatientProfile(obj);
}
function replyToReferredDoctor(index){
var obj=referralList[index];
if($('textarea#ReferredDocRemark-'+index).val()){
var body = {
 ProjectID :obj.ProjectID,
AdmissionNo:obj.AdmissionNo,
LineItemNo:obj.LineItemNo,
ReferredDoctorRemarks : $('textarea#ReferredDocRemark-'+index).val(),
EditedBy:App.user.DoctorID,
//EditedBy:41,
PatientID:obj.PatientID,
ReferringDoctor:obj.ReferringDoctor
      };
      var request = new Req.RequestModel("addReferredDocRemark", body, replyToReferredDoctorSuccess, replyToReferredDoctorError);
      Req.sendRequest(request);
	  }else{
	  App.showErrorMsg(Loc.referralPage.fillFieldsMsg);
	  }

}
function replyToReferredDoctorSuccess(response){
App.showSuccessMsg(response.ErrorEndUserMessage);
MyPatientsController.getReferrals();
}
function replyToReferredDoctorError(response){
App.showErrorMsg(response.ErrorEndUserMessage);
}
function VerifyReferralDocRemarks(index){
var obj=referralList[index];
var body = {
 ProjectID :obj.ProjectID,
AdmissionNo:obj.AdmissionNo,
LineItemNo:obj.LineItemNo,
EditedBy:App.user.DoctorID
//EditedBy:1639
      };
      var request = new Req.RequestModel("VerifyRemarks", body, VerifyReferralDocRemarksSuccess, VerifyReferralDocRemarksError);
      Req.sendRequest(request);

}
function VerifyReferralDocRemarksSuccess(response){
App.showSuccessMsg(response.ErrorEndUserMessage);
MyPatientsController.getReferred();
}
function VerifyReferralDocRemarksError(response){
App.showErrorMsg(response.ErrorEndUserMessage);
}
////////////////////////////Shaden addition //////////////////
function CalculateReferBeforeDate(){
var PriorityChecked = $("input[name*=priorityChoice]:checked").val();
var additionalHours = 0;
switch (PriorityChecked){
case "1":
additionalHours = 3;
break;
case "2":
additionalHours = 6;
break;
case "3":
additionalHours = 24;
break;
}
var BeforDate =  new Date();
 //BeforDate = dateFormat(BeforDate, "yyyy-mm-dd");
  BeforDate.setHours(BeforDate.getHours() + additionalHours); // why undefined !!!!!
  BeforDate = dateFormat(BeforDate, "yyyy-mm-dd, hh:MM TT");
 //BeforDate.setHours(BeforDate.getHours()+additionalHours); 
 BeforDate = BeforDate.toString();
 $("#ReferPatientBeforeDate").empty();
 $('#ReferPatientBeforeDate').append(BeforDate);
}

/////////////////////////////////////////////////////////////
  return {
    init: init,
goToReferralPage:goToReferralPage,
referToDoctor:referToDoctor,
displayReferrals:displayReferrals,
goToPatientProfilePage:goToPatientProfilePage,
replyToReferredDoctor:replyToReferredDoctor,
VerifyReferralDocRemarks:VerifyReferralDocRemarks,
CalculateReferBeforeDate:CalculateReferBeforeDate
  }
})();