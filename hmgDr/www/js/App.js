/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
 var gaPlugin;
 function nativePluginResultHandler(result) {}

function nativePluginErrorHandler(error) {}
var App = {
    SOME_CONSTANTS : false,  // some constant


    // Application Constructor
    initialize: function() {
        console.log("console log init");
        this.bindEvents();
        this.initFastClick();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    initFastClick : function() {
        window.addEventListener('load', function() {
            FastClick.attach(document.body);
        }, false);
    },
    // Phonegap is now ready...
		onDeviceReady: function() {
		navigator.splashscreen.hide();
		gaPlugin = window.plugins.gaPlugin;
        gaPlugin.init(nativePluginResultHandler, nativePluginErrorHandler, "UA-58439991-1", 10);
        console.log("device ready, start making you custom calls!");
		document.addEventListener("backbutton", onBackKeyDown, false);
		App.checkNet();

    },
	  // Update DOM on a Received Event
  receivedEvent: function (id) {
    var parentElement = document.getElementById(id);
    var listeningElement = parentElement.querySelector('.listening');
    var receivedElement = parentElement.querySelector('.received');
    listeningElement.setAttribute('style', 'display:none;');
    receivedElement.setAttribute('style', 'display:block;');
  }
	
};
function onBackKeyDown() {
  var loginPage = "#login-page";
  if ($.mobile.activePage.is(loginPage)) {
    if (typeof (navigator.app) !== "undefined")
      window.navigator.app.exitApp();
    else
      window.history.back();
  } else if ($.mobile.activePage.is("#home-page")) {
 $.mobile.changePage("#login-page");
  } else if ($.mobile.activePage.is("#progress-notes-page")) {
  var x= $.mobile.navigate.history.stack.length-App.profilePageIndex;
  if(x==2){
      window.history.go(-1);
  }else{
    window.history.go(-x);
  }
 // }
   } else if ($.mobile.activePage.is("#search-patient-page")) {
   $.mobile.changePage("#home-page");
  // } else if ($.mobile.activePage.is("#prescription-page") || $.mobile.activePage.is("#LabResults-page")) {
    // if (App.isUpdateEmail == true) {
      // window.history.go(-3);
    // } else {
      // window.history.go(-1);
    // }
  // }
  // //	else if ($.mobile.activePage.is("#news-page") || $.mobile.activePage.is("#custom-login-page") || 
  // // $.mobile.activePage.is("#reg-page") || $.mobile.activePage.is("#appo-page") || 
  // // $.mobile.activePage.is("#settings-page") || $.mobile.activePage.is("#careerTyps-page") || 
  // // $.mobile.activePage.is("#staff-page") || $.mobile.activePage.is("#validate-page"))  {   
  // // var x = $.mobile.urlHistory.stack.length - 1; 
  // // window.history.go(-x);
  // // } 
 } else {
    window.history.go(-1);
  }
}
// function shouldCancelBackButton() {
  // var loginPage = "#login-page";
  // if ($.mobile.activePage.is(loginPage)) {
    // return "false";
  // } else
    // return "true";
// }
// App.isMobile = {
  // Android: function () {
    // return device.platform.match(/android|Android/i);
  // },
  // BlackBerry: function () {
    // return device.platform.match(/BlackBerry/i);
  // },
  // BlackBerry10: function () {
    // return device.platform.match(/BB10/i);
  // },
  // iOS: function () {
    // return device.platform.match(/iOS|iPhone|iPad|iPod/i);
   // //return navigator.userAgent.match(/iOS|iPhone|iPad|iPod/i);
  // },
  // Opera: function () {
    // return navigator.userAgent.match(/Opera Mini/i);
  // },
  // Windows: function () {
    // return device.platform.match(/Win32NT|WinCE|IEMobile/i);
  // },
  // Browser: function () {
    // return navigator.userAgent.toLowerCase().match('chrome');
  // },
  // any: function () {
    // return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
  // }
// };
App.isMobile = {
		    Android : function() {
		        return navigator.userAgent.match(/android|Android/i);
		    },
		    BlackBerry : function() {
		        return navigator.userAgent.match(/BlackBerry/i);
		    },
		    BlackBerry10 : function() {
		        return navigator.userAgent.match(/BB10/i);
		    },
		    iOS : function() {
		        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		    },
		    Opera : function() {
		        return navigator.userAgent.match(/Opera Mini/i);
		    },
		    Windows : function() {
		        return navigator.userAgent.match(/Win32NT|WinCE|IEMobile/i);
		    },
			Browser: function () {
				return navigator.userAgent.toLowerCase().match('chrome');
			},
		    any : function() {
		        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS()
		                || isMobile.Opera() || isMobile.Windows());
		    }
		};
App.initialize();
App.deviceInternetPresent = false;
App.appBuildVersion = "0.0.1";
// App.deviceType = "";
// App.lastAccessedWebService = "N/A";
App.token = "";
App.projectsList = [];
App.user = {};
App.isUserLogged = false;
 App.profilePageIndex=0;
//App.projectsIDs = [];
// App.appDebugModeOn = true;
// App.lastErrorMessage = "0";
App.currentPatient=new Object();
App.checkNet = function checkReachability() {
  var _executeFlag = false;
  var _check = false;
  // lets only run the connection detection code on mobile devices only.
  if (App.isMobile.any()) {
    _executeFlag = true;
    if (navigator.network == undefined) {
      _check = false; // net is NOT working
      App.deviceInternetPresent = _check;
    } else {
      var networkState = navigator.network.connection.type;
      var states = {};
      states[Connection.UNKNOWN] = 'Unknown connection';
      states[Connection.ETHERNET] = 'Ethernet connection';
      states[Connection.WIFI] = 'WiFi connection';
      states[Connection.CELL_2G] = 'Cell 2G connection';
      states[Connection.CELL_3G] = 'Cell 3G connection';
      states[Connection.CELL_4G] = 'Cell 4G connection';
      states[Connection.CELL] = 'Cell generic connection';
      states[Connection.NONE] = 'No network connection';
      if (networkState == Connection.UNKNOWN || networkState == Connection.NONE) {
        _check = false; // net is NOT working
        App.deviceInternetPresent = _check;
      } else {
        _check = true; // net is working
        App.deviceInternetPresent = _check;
        //  console.log('Device network is available.');
      }
      // console.log('Device network connection type detected as: ' + states[networkState]);
    }
  } else { // if not mobile.. lets do a google ping on PC browser to check if net is working
    _executeFlag = false;
    try {
      var start = new Date().getTime();
      $.support.cors = true;
      $.mobile.allowCrossDomainPages = true;
      $.ajax({
        type: 'GET',
        crossDomain: true,
        timeout: 8000,
        url: "http://www.google.com",
        success: function (data, textStatus, XMLHttpRequest) {
          if (XMLHttpRequest.statusText == "Please login.") {
            _check = false;
            App.deviceInternetPresent = _check;
            //console.log("Internet Connectivity: Not Logged to Cyberrom In HMG, Net Blocked!");
          } else {
            _check = true;
            App.deviceInternetPresent = _check;
            //console.log("Internet Connectivity: Detected! [Status Code: " + XMLHttpRequest.status + "]");
          }
        },
        error: function (jqXHR, textStatus, errorThrown) {
          _check = false;
          App.deviceInternetPresent = _check;
          //console.log("Internet Connectivity: Not Available! " + textStatus + " [Status Code: " + jqXHR.status + "]");
        },
        complete: function (data) {
          App.deviceInternetPresent = _check;
          var end = new Date().getTime();
          var reqTime = end - start;
        }
      });
    } catch (e) {
      _check = false;
      App.deviceInternetPresent = _check;
    }
  }
  return _check;
}
App.randomString = function () {
  var result = '';
  var length = 10;
  var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
  return result;
}
App.ConvertDate = function (value) {
var dt=value;
if(dt){
	  dt=new Date(parseInt(dt.substring(6,dt.length - 2)));
		dt= dateFormat(dt, "dd/mm/yyyy");	  
	  }
  return dt;
}
App.SetProjectIDForRequest = function () {
var projID=0;
var patientType=App.currentPatient.PatientStayType;
if(patientType){
 if(patientType==2){  
		if(App.user.ProjectID==2){
		projID=App.currentPatient.ProjectID;
		}else{
			projID=0;
			}  
	  }else{
	  projID=App.currentPatient.ProjectID;
	  }
	  }
  return projID;
}
App.Beep = function Beep() {
  if (typeof navigator === 'object' && typeof navigator.notification === 'object') {
    navigator.notification.beep(1);
    navigator.notification.vibrate(2000);
  }
}
App.ShowUIBlocker = function ShowUIBlocker() {
     $.mobile.loading( "show", {
            text: "",
            textVisible: false,
            html: ""
    });
}
App.HideUIBlocker = function HideUIBlocker() {
	 $.mobile.loading( "hide" );
}
App.dialNumber = function (number) {
  if (isMobile.Android() || isMobile.Windows()) {
    document.location.href = "tel:" + number;
  } else if (isMobile.iOS()) {
    phonedialer.dial(number);
  }
}
App.showSuccessMsg = function (msg, callback) {
  var back = (callback) ? callback : function () {};
  if (typeof navigator === 'object' && typeof navigator.notification === 'object') {
    navigator.notification.alert(msg, back, Loc.msg.success, Loc.msg.ok);
    //App.Beep();
  } else {
    alert(msg);
    back();
  }
}
App.showErrorMsg = function (msg, callback) {
  var back = (callback) ? callback : function () {};
  if (typeof navigator === 'object' && typeof navigator.notification === 'object') {
    navigator.notification.alert(msg, back, Loc.msg.msgAlert, Loc.msg.ok);
    //App.Beep();
  } else {
    alert(msg);
    back();
  }
}
App.showInfoMsg = function (msg, callback) {
  var back = (callback) ? callback : function () {};
  if (typeof navigator === 'object' && typeof navigator.notification === 'object') {
    navigator.notification.alert(msg, back, Loc.msg.info, Loc.msg.ok);
   // App.Beep();
  } else {
    alert(msg);
    back();
  
  }
}
App.setBackPage=function(btn,pageID){
 $(btn).attr("href", pageID);
}
App.calculateInpatientStayPeriod=function(CalFromDate , CalToDate){
  CalToDate = dateFormat(CalToDate, "yyyy/mm/dd");
  CalFromDate = dateFormat(CalFromDate, "yyyy/mm/dd");
  CalToDate = CalToDate.toString();
  CalFromDate = CalFromDate.toString();
  CalToDate = CalToDate.split('/');
  CalFromDate = CalFromDate.split('/');
  var Num = 0;
  var FromDate = new Date(CalFromDate[0], CalFromDate[1]-1, CalFromDate[2]);
  var ToDate = new Date(CalToDate[0], CalToDate[1]-1, CalToDate[2]);
  if (ToDate >= FromDate) {
            Num = new Date(ToDate - FromDate);
            Num = (Num.getTime() / 86400000)+1;//add one day to include the admission day
        }
 return Num;
 }
App.setUserSignIn = function (doctor) {
  if (doctor.DoctorName === null)
    doctor.DoctorName = "";
  if (doctor.EmailAddress == null || doctor.EmailAddress == "" || doctor.EmailAddress == " ")
    doctor.EmailAddress = "unknown@unknown.com";
 App.isUserLogged = true;
App.user=doctor;
$.jStorage.set("hmg.user.current",doctor);
}
App.setUserSignOut = function () {
  if (App.isUserLogged == true) {
    App.isUserLogged = false;  
  }
  App.isUserLogged = false;
  App.user={};
  Req.token = "";
  $.jStorage.deleteKey('hmg.user.current');
  $.mobile.changePage("#login-page");
}
App.GetPageTitleForGA = function () {
    title = $.mobile.activePage.attr('data-title');
  return title;
}
App.showAppUpdateMsg = function (msg, updateCallBack, cancelCallBack) {
    var upCallBack = (updateCallBack) ? updateCallBack : function () {
    
        if (isMobile.Android())
        {
            window.open('https://play.google.com/store/apps/details?id=com.hmg.hmgDr', '_system');    
        }
        else if(isMobile.iOS())
        {
            window.open('https://itunes.apple.com/app/dr.-sulaiman-al-habib-medical/id935273167', '_system');   
        }
        else if(isMobile.Windows())
        {
            window.open('http://www.windowsphone.com/en-us/store/app/hmgdoctor/54269fe1-818d-4bf8-9d5e-d14b632baae5', '_system');   
        }
    
    };
    var cancelBack = (cancelCallBack) ? cancelCallBack : function () {};
    var buttons = [Loc.msg.cancel, Loc.msg.update];
    var onConfirm = function (i) {
        if (i == 1) {
            cancelBack();
 
        } else {
            upCallBack(); 
        }
    }
    if (typeof navigator === 'object' && typeof navigator.notification === 'object') {
        navigator.notification.confirm(
            msg, // message
            onConfirm, // callback to invoke with index of button pressed
            Loc.msg.updateTitle, // title
            buttons // buttonLabels
        );
    } else {
        if (confirm(msg)) {
            upCallBack();
        } else {
            alert(msg);
        }
    }
}
// App.getUserDetails = function () {
  // return $.jStorage.get("hmg.user.current");
// }
// App.setUserDetails = function (userId, userMobile, patientID, patientType, patientName, patientNameAR, emailAddress, PatientOutSA, userProjectID, birthDate) {
  // $.jStorage.set("hmg.user.current", {
    // id: userId,
    // mobile: userMobile,
    // patientID: patientID,
    // name: patientName,
    // nameAR: patientNameAR,
    // type: patientType,
    // emailAddress: emailAddress,
    // isOutSA: PatientOutSA,
    // projectID: userProjectID,
    // dateOfBirth: birthDate
  // });
// }
$(document).bind("mobileinit", function () {

  (function ($) {
    var pageOrientation = "portrait";
  
    var _oldChangePage = $.mobile.changePage;
    $.mobile.changePage = function (to, options) {
      return _oldChangePage(to, options);
    };
	  $('div:jqmData(role="page")').on('pageshow', function () {
      var title;
      title = App.GetPageTitleForGA();
      if (title) {
        if (!App.isMobile.Browser() && !App.isMobile.Windows()) {
          gaPlugin.trackPage(nativePluginResultHandler, nativePluginErrorHandler, title);
        }
		 if (App.isMobile.Android()) {
          gaPlugin.trackPage(nativePluginResultHandler, nativePluginErrorHandler, title);
        }
      }
    });
  })(jQuery);
 
   App.token = App.randomString();
   $('#mainBody').show();
  //init  language and set RTL style
  if (App.LANG == "en") {
    Loc = enLoc;
    Loc.Dir = "ltr";
  } else {
    Loc = arLoc;
    Loc.Dir = "rtl";
  }
    App.isUserLogged = ($.jStorage.get("hmg.user.isChangingLang") == true) ? true : false;
  $.jStorage.set("hmg.user.isChangingLang", false);
  if (App.isUserLogged == true) {
    App.token = $.jStorage.get("hmg.user.session");
    $.jStorage.set("hmg.user.session", "");
    Req.token = $.jStorage.get("hmg.user.token");
    $.jStorage.set("hmg.user.session", App.token);
    $.jStorage.set("hmg.user.token", "");
	App.user=$.jStorage.get("hmg.user.current");
  }
   $(document).ajaxStop(function () {
    App.HideUIBlocker();
  });
    $('input[data-role=date]').on('focus', function () {
	this.blur();
	});
	 if (App.isUserLogged == true) {
      document.location.hash = "#home-page";
  } else {
    document.location.hash = "#login-page";
  }
	HomeController.init();
   SettingsController.init();
   MyPatientsController.init();
   PatientProfileController.init();
   ProgressNotesController.init();
   VitalSignsController.init();
   PrescriptionController.init();
   DoctorInfoController.init();
   LabResultsController.init();
   SearchMedicineController.init();
   ValidationController.init();
   MapController.init();
   LoginController.init();
   RadiologyController.init();
   DrProfileController.init();
   DoctorReplyController.init();
   ApprovalController.init();
	ReferralController.init();
	BloodBankController.init();
});
(function ($) {
  $.fn.disableButton = function () {
    return this.each(function () {
      $this = $(this);
      if ($this.is('a')) {
        $this.addClass("ui-state-disabled");
        return;
      }
      if ($this.is('input')) {
        $this.button("disable");
        return;
      }
	   if ($this.is('button')) {
        $this.addClass("ui-state-disabled");
        return;
      }
    });
  };
  $.fn.enableButton = function () {
    return this.each(function () {
      $this = $(this);
      if ($this.is('a')) {
        $this.removeClass("ui-state-disabled");
        return;
      }
      if ($this.is('input')) {
        $this.button("enable");
        return;
      }
	    if ($this.is('button')) {
		$this.removeClass("ui-state-disabled");
        return;
      }
    });
  };
})(jQuery);