var MapController = MapController || {};
var map;
MapController = (function () {
  var mapPageSel = "#map-page";
  var markersArray = [];
  var GeoMarker;
  var selectedMarkerIndex = 0;
  var directionsRenderer;
  var list;
  var ids;
  var firstTime = false;
  var locationsList = [];
  var GPSError = false;
  var filter = [false // not used
    , true //Project 
    , true //Pharmacy
  ];
  var markersColors = ['E81B2C' // not used
    , 'red' //Project 
    , 'green' //Pharmacy
  ];

  function init() {
    $(mapPageSel).on("pagebeforeshow", function () {
      $("#createRouteBtn").show();
      $("#clearRouteBtn").hide();
    if (GPSError == true) {
        MapController.addGeoMarker();
        GPSError = false;
     }
    });
	 $(mapPageSel).on("pageshow", function () {
		//refreshMarkers();
		 console.log("hit: pageshow()");
		clearRouts();
    });
  } // init()
  function getMapLocation(index,medicineLocationList){
  locationsList=medicineLocationList;
  selectedMarkerIndex=index;
  loadMapApi();
  
  }
  function loadMapApi() {
    if (locationsList && locationsList.length > 0) {
      loadMapAPI();
    } else {
      retriveLocations();
    }
  }

  function loadMapAPI() {
    if (typeof google === 'object' && typeof google.maps === 'object') {
      $.mobile.changePage("#map-page");
    } else {
      var element = document.createElement('script');
      element.src = 'http://maps.googleapis.com/maps/api/js?sensor=true&callback=MapController.Initialize';
      element.type = 'text/javascript';
      var scripts = document.getElementsByTagName('script')[0];
      scripts.parentNode.insertBefore(element, scripts);
      $.mobile.changePage("#map-page");
    }
  }

  function retriveLocations() {
    var body = {};
    var request = new Req.RequestModel("getLocations", body, function (response) {
      locationsList = response.ListHMGLocation;
      loadMapAPI();
    }, function (response) {
      App.showErrorMsg(response.ErrorEndUserMessage);
    });
    Req.sendRequest(request);
  }

  function Initialize() {
    google.maps.visualRefresh = true;
    var MapOptions = {
      zoom: 10,
      center: new google.maps.LatLng(24.719364, 46.657567),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      sensor: true
    };
    map = new google.maps.Map(document.getElementById("mapCanvas"), MapOptions);
    //Delay customizations until the map has loaded
    var element = document.createElement('script');
    element.src = 'libs/geolocationmarker.js';
    element.type = 'text/javascript';
    var scripts = document.getElementsByTagName('script')[1];
    scripts.parentNode.insertBefore(element, scripts);
    list = {};
    var len = locationsList.length;
    ids = [];
    for (var i = 0; i < len; i++) {
      var item = locationsList[i];
      list[i] = item;
      ids.push(item.LocationID);
    }
  }

  function addGeoMarker() {
    //firstTime = true;
    GeoMarker = new GeolocationMarker();
    GeoMarker.setCircleOptions({
      fillColor: '#808080'
    });
    google.maps.event.addListenerOnce(GeoMarker, 'position_changed', function () {
      map.setCenter(this.getPosition());
      map.fitBounds(this.getBounds());
      // if (firstTime == true) {
        // nearest(this.getPosition().lat(), this.getPosition().lng());
        //refreshMarkers();
        // firstTime = false;
      // }
    });
    google.maps.event.addListener(GeoMarker, 'geolocation_error', function (e) {
      //TODO:
      GPSError = true;
     // App.showErrorMsg(Loc.msg.locationError);
    });
    GeoMarker.setMap(map);
    directionsRenderer = new google.maps.DirectionsRenderer();
    directionsRenderer.setMap(map);
    refreshMarkers();
  }

  function refreshMarkers() {
    for (var i = 0; i < markersArray.length; i++) {
      markersArray[i].setMap(null);
    }
    markersArray.length = 0;
    var item;
    var pos;
    for (var i = 0; i < ids.length; i++) {
      item = list[i];
      pos = new google.maps.LatLng(item.Latitude, item.Longitude);
      //if (filter[item.LocationType] && filter[item.LocationType] == true) {
        if (selectedMarkerIndex == i) {
          addSelectedMarker({
            position: pos,
            type: item.LocationType
          });
          selectedMarker = item;
          $("#selectedMarkerName").text(item.LocationDescription);
        } else {
          addMarker({
            position: pos,
            index: i,
            type: item.LocationType
          });
        }
      //}
    }
  }

  function addMarker(options) {
    var color = markersColors[options.type]
    var marker = new google.maps.Marker({
      map: map,
      draggable: false,
      icon: {
        path: google.maps.SymbolPath.CIRCLE,
        fillOpacity: 1,
        fillColor: color,
        strokeOpacity: 1.0,
        strokeColor: color,
        strokeWeight: 3.0,
        scale: 10 //pixels
      },
      position: options.position
    });
    google.maps.event.addListener(marker, 'click', function () {
      markerClicked(options.index)
    });
    markersArray.push(marker);
  }

  function addSelectedMarker(options) {
    var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + markersColors[0]);
    var marker = new google.maps.Marker({
      map: map,
      draggable: false,
      icon: pinImage,
      position: options.position
    });
    markersArray.push(marker);
  }

  function markerClicked(index) {
    selectedMarkerIndex = index;
    refreshMarkers();
  }

  function routeToSelectedMarker() {
 //   GATrackEvent("Button", "Click", "Directions", 1);
    if (GeoMarker.getPosition()) {
      $("#clearRouteBtn").show();
      $("#createRouteBtn").hide();
      for (var i = 0; i < markersArray.length; i++) {
        markersArray[i].setMap(null);
      }
      markersArray.length = 0;
      directionsRenderer.setDirections({
        routes: []
      });
      var directionsService = new google.maps.DirectionsService();
      var request = {
        origin: GeoMarker.getPosition(),
        destination: new google.maps.LatLng(selectedMarker.Latitude, selectedMarker.Longitude),
        travelMode: google.maps.DirectionsTravelMode.DRIVING,
        unitSystem: google.maps.DirectionsUnitSystem.METRIC
      };
      directionsService.route(request, function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
          directionsRenderer.setDirections(response);
	var textDis=Loc.mapPage.dis +response.routes[0].legs[0].distance.text;
		  var textDur=Loc.mapPage.dur+ response.routes[0].legs[0].duration.text;
		    var allDetails=textDis+" / "+textDur;
		    $("#dirDetails").empty().append(allDetails);
	
        } else {
          //TODO:
          App.showErrorMsg(Loc.msg.locationError);
        }
      });
    } else {
      App.showInfoMsg(Loc.msg.NoPosition);
    }
  }

  function clearRouts() {
  	 $("#dirDetails").empty();
    $("#createRouteBtn").show();
    $("#clearRouteBtn").hide();
    directionsRenderer.setDirections({
      routes: []
    });
    refreshMarkers();
  }

  function rad(x) {
    return x * Math.PI / 180;
  }

  function nearest(lat, lng) {
    var R = 6371; // radius of earth in km
    var distances = [];
    var closest = -1;
    var markers = markersArray;
    var i = ids.length - 1;
    do {
      var item = list[ids[i]];
      var mlat = item.Latitude;
      var mlng = item.Longitude;
      var dLat = rad(mlat - lat);
      var dLong = rad(mlng - lng);
      var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(rad(lat)) * Math.cos(rad(lat)) * Math.sin(dLong / 2) * Math.sin(dLong / 2);
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
      var d = R * c;
      distances[i] = d;
      if (closest == -1 || d < distances[closest]) {
        closest = i;
      }
    } while (i--);
    selectedMarkerIndex = closest;
  }

  function dialBrancheNumber() {
  //  GATrackEvent("Button", "Click", "Call Selected Location", 1);
    App.dialNumber(list[selectedMarkerIndex].PhoneNumber);
  }

  function openFilterDiv() {
    var mapWidth = $("#map-page").width();
    $("#popupPanelLoc-popup").attr("style", $("#popupPanelLoc-popup").attr("style") + "; width:" + mapWidth + "px");
  //  $("#popupPanelLoc").popup();
    $("#popupPanelLoc").popup("open");
  }
  return {
    init: init,
    loadMapApi: loadMapApi,
    Initialize: Initialize,
    addGeoMarker: addGeoMarker,
    routeToSelectedMarker: routeToSelectedMarker,
    clearRouts: clearRouts,
    dialBrancheNumber: dialBrancheNumber,
    openFilterDiv: openFilterDiv,
	getMapLocation:getMapLocation
  }
})();