var DoctorInfoController = DoctorInfoController || {};
DoctorInfoController = (function () {
  function init() {
  } // init()
     function getDrSchedule() {

      var body = {
        ProjectID: App.user.ProjectID,
		ClinicID:App.user.ClinicID,
		DoctorID:App.user.DoctorID,
		DoctorWorkingHoursDays :7
      };
      var request = new Req.RequestModel("doctorSch", body, getDrScheduleSuccess, getDrScheduleError);
      Req.sendRequest(request);
  }
    function getDrScheduleSuccess(response){
  $('#doctorSchDiv').empty();
  var list=response.List_DoctorWorkingHoursTable;
    var html = "";
    html =  '<table align="center" class="tableTheme" id="drScheduleTable" ><thead><tr><th style="text-align:center;">'+Loc.drSchPage.date+'</th><th class="ResultValue">'+Loc.drSchPage.day+'</th><th>'+Loc.drSchPage.workingHr+'</th></tr></thead><tbody >'
    for (var i = 0; i < list.length; i++) {
      var item = list[i];
	  var dt=item.Date;
	  dt=new Date(parseInt(dt.substring(6,dt.length - 2))) 
      html += '<tr id="row-' + i + '" class="results">' + '<td class="rowDescription" style="text-align:center;">' + dateFormat(dt, "dd/mm/yyyy") + '</td>' + '<td class="rowResultValue">' + item.DayName + '</td>' + '<td class="rowReferanceRange">' + item.WorkingHours + '</td>' + '</tr>';
    }
    html += '</tbody></table>';
    $('#doctorSchDiv').append(html);
	 $.mobile.changePage("#drSchedule-page");
  }
    function getDrScheduleError(response){
	App.showErrorMsg(response.ErrorEndUserMessage);
  }
  return {
    init: init,
	getDrSchedule:getDrSchedule
  }
})();