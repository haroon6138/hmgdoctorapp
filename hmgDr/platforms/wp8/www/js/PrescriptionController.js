var PrescriptionController = PrescriptionController || {};
PrescriptionController = (function () {
 var presPage = "#prescription-page";
 // var presPage = "#prescription-page";
  var presList = [];
  var prescriptionList = [];
  var imgsArray = ["img/dr_m.png", "img/dr_f.png"];
  var userProfile = "";
  var errorMsg = "";
  var prescIndex = 0;
  var currentPres;
  var patientInfo;
  /*************** initiation****************************/
  function init() {
     
      $("#prescription-page").on("pagebeforeshow", function () {
      $("#presList").empty();
     if (presList.length > 0) {
        displayPresList();
      } else { 
        DisplayNoRecordMessage();
     } 
    });  
      $("#prescriptionDetails-page").on("pagebeforeshow", function () {
	  displayPresDetailsList();
    });  	  
     // $(presPage).on("pagebeforeshow", function () {
      // $("#prescription-list-set").empty();
      // prescriptionList.length = 0;
      // var body = {
        // ProjectID: tempPresAppo.ProjectID,
        // AppointmentNo: tempPresAppo.AppointmentNo,
        // EpisodeID: tempPresAppo.EpisodeID,
        // SetupID: tempPresAppo.SetupID,
        // PatientID: App.currentPatient.patientID,
		// PatientTypeID:App.currentPatient.PatientType
      // };
      // var request = new Req.RequestModel("prescriptionList", body, retrievePrescriptionSuccess, retrievePrescriptionError);
      // Req.sendRequest(request);
    // });

  } // init()
  /*******************prescription appointment**********************/

  function displayPresList() {
  
    var list = presList;
	 if(App.currentPatient.PatientStayType==1){
	 var date;
    $("#presList").empty();
    var html = "";
    for (var i = 0; i < list.length; i++) {
      var item = list[i];
      date = item.OrderDate;
      date = App.ConvertDate(date);
	  html+='<li style="direction:ltr;text-align:left;" data-icon="next" data-iconpos="right">' + '<a class="inpatientPresList"  onclick="PrescriptionController.displayPresDetails(' + i + ')">' +'<div class="appoDrName">' + item.ItemDescription + '</div>' +'<div style="font-size:12px; color:#ed1b2c;">' + '<span style="font-size:12px; color:gray;">'+Loc.precriptionPage.createdBy+': </span>'+item.CreatedByName + '</div>'+'</a>'+'</li>';
     }
	 }else{
    var date;
    $("#presList").empty();
    var html = "";
    for (var i = 0; i < list.length; i++) {
      var item = list[i];
      date = item.OrderDate;
      date = App.ConvertDate(date);
      html += '<li>' + '<div "style="border:0px; background-color:white;"  onclick="PrescriptionController.displayPresDetails(' + i + ')">' + '<table width="100%" border="0" cellspacing="3px"> <thead class="ui-widget-header"> </thead><tbody class="">' + '<tr class="presAppoDetails" >' + '<td style="width:5%;vertical-align: top;" rowspan="3">' + '<div>' + '<img style="border:1px solid;margin-top: 3px;" width="70px" height="80px" src="' + item.DoctorImageURL + '" onError="this.onerror=null;this.src=\'' + imgsArray[0] + '\';" align="middle" />' + '</div>' + '</td>' + '<td style="" colspan="2">' + '<div>' + '<div class="appoDrName">' + item.ItemDescription + '</div>' + '<div style="font-size:12px; color:#ed1b2c;">' + item.DoctorName + '</div>' + '</div>' + '</td>' + '<td  rowspan="3" align="center" class="arrow-icon">' + '</td>' + '</tr>' + '<tr class="presAppoDetails" >' + '<td>' + '<img class="presSmallImage" src="img/calendar.png" width="30px" height="30px">'+ "  " + date + '</td>'  +'</tr> <tr class="presAppoDetails"> <td style="vertical-align: top;">' + '<img class="presSmallImage" style="top:-7px;" src="img/pin.png" width="30px" height="30px">' + "  " + item.Clinic + '</td>' + '</tr>' + '</tbody></table>' + '</div>' + '</li>';
    }
	}
    $("#presList").append(html).listview('refresh');
  }


  /*************** Pres Details Screen ***********************/
  function displayPresDetails(index) {
    tempPresAppo = presList[index];
    prescIndex = index;
    $.mobile.changePage("#prescriptionDetails-page");
  }

  function retrievePrescriptionSuccess(response) {
    prescriptionList = response.ListPRM;
    displayPresDetailsList();
  } 

   function retrievePrescriptionError(response) {
    App.showErrorMsg(response.ErrorEndUserMessage);
  } 

   function displayPresDetailsList() {
   
    // var list = prescriptionList;
    $("#prescription-list-set").empty();
     var html = "";
     var innerHtml = "";
     var prescription;
	 prescription=tempPresAppo;
    // for (var i = 0; i < list.length; i++) {
      // prescription = list[i];
 if(App.currentPatient.PatientStayType==1){
       innerHtml = '<h3>' + prescription.ItemDescription +'</h3>' + '<ul data-role="listview" class="prescription-list" style="margin:0px;direction: ltr;text-align:left!important;" id="presList-1">' +
                  '<li>' +'<table width="100%" cellspacing="0" class="prescriptionListTableDetail" style="direction:ltr !important;text-align: left;">'
						 +'<tr><td>'+ 'Direction' +'</td><td class="redColorForText">'+ prescription.Direction +'</td></tr>'
					     +'<tr><td>'+ 'Refill' +'</td><td class="redColorForText">'+ prescription.RefillType + '</td></tr>'
						 +'<tr><td>'+ 'Dose' +'</td><td class="redColorForText">'+ prescription.Dose +'</td></tr>'
						 +'<tr><td>'+ 'UOM' +'</td><td class="redColorForText">'+prescription.UnitofMeasurementDescription +'</td></tr>'
						 +'<tr><td>'+ 'Start Date' +'</td><td class="redColorForText">'+App.ConvertDate(prescription.StartDatetime) +'</td></tr>'
						 +'<tr><td>'+ 'Stop Date' +'</td><td class="redColorForText">'+App.ConvertDate(prescription.StopDatetime) +'</td></tr>'
						 +'<tr><td>'+ 'No of Doses' +'</td><td class="redColorForText">'+prescription.NoOfDoses +'</td></tr>'
						 +'<tr><td>'+ 'Route' + '</td><td class="redColorForText">'+ prescription.Route +'</td></tr>'
						 +'<tr><td>'+ 'Comments' +'</td><td class="redColorForText">'+prescription.Comments +'</td></tr>'
						 +'<tr><td>'+ 'Pharmacy Remarks' +'</td><td class="redColorForText">'+prescription.Pharmacy_Remarks +'</td></tr>'
						 +'<tr><td>'+ 'Prescription Date' +'</td><td class="redColorForText">'+App.ConvertDate(prescription.PrescriptionDatetime)+'</td></tr>'
 						 +'<tr><td>'+ 'Status' +'</td><td class="redColorForText">'+prescription.StatusDescription +'</td></tr>'
						 +'<tr><td>'+ 'Created By' +'</td><td class="redColorForText">'+prescription.CreatedByName +'</td></tr>'
						 +'<tr><td>'+ 'Processed By' +'</td><td class="redColorForText">'+prescription.ProcessedBy +'</td></tr>'
						 +'<tr><td>'+ 'Authorized By' +'</td><td class="redColorForText">'+prescription.AuthorizedBy +'</td></tr>'
 + '</table>' + '</li>' + '</ul>';
 }else{
      innerHtml = '<h3>' + prescription.ItemDescription +'</h3>' + '<ul data-role="listview" class="prescription-list" style="margin:0px;" id="presList-1">' +
                  '<li>' +'<table width="100%" cellspacing="0" class="prescriptionListTableDetail">'
                         +'<tr><td>'+ Loc.precriptionPage.route + '</td><td class="redColorForText">'+ prescription.Route +'</td></tr>'
						 +'<tr><td>'+ Loc.precriptionPage.freqTiming +'</td><td class="redColorForText">'+ prescription.Frequency +'</td></tr>'
					     +'<tr><td>'+ Loc.precriptionPage.insCovered +'</td><td class="redColorForText">'+ prescription.IsCovered + '</td></tr>'
						 +'<tr><td>'+ Loc.precriptionPage.durationDays +'</td><td class="redColorForText">'+ prescription.Days +'</td></tr>'
						 +'<tr><td>'+ Loc.precriptionPage.doctorRemarks +'</td><td class="redColorForText">'+prescription.Remarks +'</td></tr>'
             + '</table>' + '</li>' + '</ul>';
}
      // if (i == 0) {
        // var html = $('<div data-role="collapsible"> </div>');
      // } else {
        var html = $('<div data-role="collapsible" ></div>');
      //}
      var listId = "#presList-1" ;
      html.append(innerHtml);
      $('#prescription-list-set').empty().append(html);
      $(listId).listview();
      $(listId).listview('refresh', true);
      html = "";
   // }
    //$('#prescription-list-set').collapsibleset('refresh');
	$('#prescription-list-set').collapsibleset().trigger('create');
  } 


  /**********************************************************/
  function DisplayNoRecordMessage() {
    $("#presList").empty();
    var html = "";
    var requestType = 0;
    var listId = "#list-1";
    html = '<li><div width="320px" style="padding: 10px;border:1px solid #c74b45;background-color:white;white-space: normal;">'
    if (App.currentPatient.PatientStayType == 2) {
      html += '<div>' + Loc.msg.NoDataYet + '</div>';
    } else {
      html += '<div>' + Loc.msg.noResults + '</div>';
    }
    html += '</div></li>';
    $('#presList').append(html).listview('refresh');
  }
  
  function setPrescriptionList(obj){
  presList=obj;
  $.mobile.changePage("#prescription-page"); 
  }

  
  return {
    init: init,
    displayPresDetails: displayPresDetails,
	setPrescriptionList: setPrescriptionList
  }
})();