var ValidationController = ValidationController || {};
ValidationController = (function () {
  var loginPageSel = "#login-page";
  var validationPageSel = "#validation-page";
  var validationBtSel = '#validateBtn';
  var code = '#validationCodeInput';
  var successMethod;
  var errorMethod;
  var userId;
  var userProjectID;
  var noOfTries;
  var loginToken;
  var myVar;
  var isSilentVal=false;
  var countdown = document.getElementById("countdown");
function init() {
    $(validationPageSel).on("pagebeforeshow", function () {
      $(code).attr("value", "");
      $(code).val("");
      $(validationBtSel).disableButton();
      setFocus("validationCodeInput");
      noOfTries = 0;
      countdown.innerHTML = "03 : 00";
      var hint = document.getElementById("hint");
     // hint.innerHTML = Loc.msg.validationMsg1 + userMobile.substr(userMobile.length - 4) + Loc.msg.validationMsg2;
	 hint.innerHTML = "";
      clearInterval(myVar);
    });
    $(validationPageSel).on("pageshow", function () {
      var target_date = new Date();
      target_date.setMinutes(target_date.getMinutes() + 3);
      // variables for time units
      var days, hours, minutes, seconds;
      setFocus("validationCodeInput");
      // update the tag with id "countdown" every 1 second
      myVar = setInterval(function () {
        var current_date = new Date().getTime();
        var seconds_left = (target_date - current_date) / 1000; // find the amount of "seconds" between now and target   
        // do some time calculations
        days = parseInt(seconds_left / 86400);
        seconds_left = seconds_left % 86400;
        hours = parseInt(seconds_left / 3600);
        seconds_left = seconds_left % 3600;
        minutes = parseInt(seconds_left / 60);
        seconds = parseInt(seconds_left % 60);
        if (minutes <= 0 && seconds <= 0) {
          countdown.innerHTML = "00:00";
          window.history.go(-1);
          clearInterval(myVar);
        }
        // format countdown string + set tag value
        if (minutes < 10) {
          minutes = "0" + minutes;
        }
        if (seconds < 10) {
          seconds = "0" + seconds;
        }
        countdown.innerHTML = minutes + " : " + seconds;
      }, 1000);
    });
    $(code).on("keyup", function () {
      validateUserInputCode();
    });
  } // init()
  
  function HasArabicCharacters(inputdata) {
	var arregex =/[\u0600-\u065F\u066A-\u06EF\u06FA-\u06FF]/;
	var isArabicAlpha=arregex.test(inputdata);
		if(isArabicAlpha)
			return true;
		else
			return false;
   }

   function HasArabicNumbers(inputdata) {
    var arNumbers = /^[\u0660-\u0669]{10}$/;
	if (arNumbers.test(inputdata))
			return true;
		else
			return false;
   }
    function validateUserInputCode() {
    var codeVal = $(code).val();
    if (codeVal != "" && codeVal.length == 4) {
      $(validationBtSel).enableButton();
    } else if (codeVal.length > 4) {
      $(code).val(codeVal.slice(0, codeVal.length - 1));
    } else {
      $(validationBtSel).disableButton();
    }
  }

  function setFocus(id) {
    var v = "#" + id;
    $(v).focus();
  }

  function validate() {
      checkValidate($(code).val());
  }

  function checkValidate(val) {
    var body = {
      activationCode: val,
      DoctorID: userId,
      LogInTokenID: loginToken,
	  ProjectID:userProjectID
    };
    var request = new Req.RequestModel("smsAuth", body, validateSuccess, validateError);
    Req.sendRequest(request);
  }

  function validateSuccess(response) {
    isSilentVal=false;
    sucessMethod(response);
  }

  function validateError(response) {
    noOfTries += 1;
    if (noOfTries > 2) {
      errorMethod(response);
    }else if(isSilentVal==true){
	App.showErrorMsg(response.ErrorEndUserMessage);
		}else {
      $(code).val("");
      $(validationBtSel).disableButton();
      App.showErrorMsg(response.ErrorEndUserMessage);
    }
  }

  function startValidation(logintoken, id,projID, success, error) {
    sucessMethod = success;
    errorMethod = error;
    userId = id;
    loginToken = logintoken;
	userProjectID=projID;
	isSilentVal=false;
    $.mobile.changePage("#validation-page");
  }

   function silentValidation(logintoken, id,projID, success, error) {
    sucessMethod = success;
    errorMethod = error;
    userId = id;
	userProjectID=projID;
    loginToken = logintoken;
	isSilentVal=true;
    checkValidate("0000");
  }

     return {
    init: init,
	HasArabicNumbers:HasArabicNumbers,
	HasArabicCharacters:HasArabicCharacters,
    validate: validate,
    startValidation: startValidation,
    silentValidation: silentValidation
  }
})();