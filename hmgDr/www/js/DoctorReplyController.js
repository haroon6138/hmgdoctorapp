var DoctorReplyController = DoctorReplyController || {};
DoctorReplyController = (function () {
var patientQuesList;
var question=[];
function init() {
$("#patientQuestions-page").on("pagebeforeshow", function () {
if(patientQuesList){
displayQuestionsList();
}
});
$("#doctorReply-page").on("pagebeforeshow", function () {
$('textarea#replyTextArea').val("");

});
}//init
function getPatientsQuestions(){

      var body = {
        ProjectID: App.user.ProjectID,
		DoctorID:App.user.DoctorID,
		TransactionNo:0
      };
      var request = new Req.RequestModel("patientQues", body, getQuestionsSuccess, getQuestionsError);
      Req.sendRequest(request);

}  
 function getQuestionsSuccess(response){
 patientQuesList=response.List_GtMyPatientsQuestions;
	$.mobile.changePage("#patientQuestions-page"); 
  }
 
function getQuestionsError(response){
	App.showErrorMsg(response.ErrorEndUserMessage);
  }
    function displayQuestionsList() {
   
     var list=patientQuesList;
    $("#patQues-list-set").empty();
     var html = "";
     var innerHtml = "";
     var question;
	 var date;
	 var replyButton='';
     for (var i = 0; i < list.length; i++) {
       question = list[i];
	     date = question.CreatedOn;
      date = App.ConvertDate(date);
	  if(question.IsVidaCall==false){
	  replyButton='<tr><td colspan="2"><input data-theme="custom-button-all" type="button" data-icon="reply"  id="addReplyButton-' + (i + 1)+'" onclick="DoctorReplyController.goToReplyPage('+i+');" value="'+Loc.doctorReplyPage.reply+'"></input></td></tr>';
	  }else{
	  replyButton="";
	  }
      innerHtml = '<h3>' + question.PatientName +'</h3>' + '<ul data-role="listview" class="question-list" style="margin:0px;list-style: none;" id="quesList-'+i+'">' +
                  '<li>' +'<table width="100%" cellspacing="0" class="prescriptionListTableDetail">'
                         +'<tr>'+'<td  colspan="2">'+ question.Remarks +'</td></tr>'
						 +'<tr><td class="redColorForText">'+ Loc.doctorReplyPage.date +'</td>'+'<td >'+ date +'</td></tr>'
						  +'<tr><td class="redColorForText">'+ Loc.doctorReplyPage.time +'</td><td>'+ question.RequestTime +'</td></tr>'
						 +'<tr><td class="redColorForText">'+ Loc.doctorReplyPage.file +'</td><td>'+question.PatientID +'</td></tr>'
						  +'<tr><td class="redColorForText">'+ Loc.doctorReplyPage.mobile +'</td><td>'+question.MobileNumber +'</td></tr>'
						 +replyButton
             + '</table>' + '</li>' + '</ul>';
        var html = $('<div data-role="collapsible" ></div>');
      var listId = "#quesList-"+i ;
      html.append(innerHtml);
      $('#patQues-list-set').append(html);
	  $("#addReplyButton-"+(i+1)).button().button('refresh');
      $(listId).listview();
      $(listId).listview('refresh', true);
      html = "";
   }
    $('#patQues-list-set').collapsibleset('refresh');
  } 
  function goToReplyPage(index){
  question=[];
  question=patientQuesList[index];
  $("#requestLabel").empty().text(Loc.doctorReplyPage.ques+" "+question.Remarks);
  $.mobile.changePage("#doctorReply-page"); 
  
  }
  function replyToRequest(){
  var replyText=$('textarea#replyTextArea').val();
  if(replyText){
  sendDoctorReply(replyText);
  }else{
App.showSuccessMsg(Loc.doctorReplyPage.replyMsg);
  }
  }
  function sendDoctorReply(response){
   var body = {
        ProjectID: question.ProjectID,
		DoctorID:question.DoctorID,
		SetupID:question.SetupID,
		TransactionNo :question.TransactionNo, 
		DoctorResponse :response
      };
      var request = new Req.RequestModel("addresp", body, sendDoctorReplySuccess, sendDoctorReplyError);
      Req.sendRequest(request);
  }
  function sendDoctorReplySuccess(){
	getPatientsQuestions();
  }
  function sendDoctorReplyError(response){
  App.showErrorMsg(response.ErrorEndUserMessage);
  }
  return {
    init: init,
	getPatientsQuestions:getPatientsQuestions,
	goToReplyPage:goToReplyPage,
	replyToRequest:replyToRequest,
  }
})();