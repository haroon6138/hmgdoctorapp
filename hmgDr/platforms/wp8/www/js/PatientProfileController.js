var PatientProfileController = PatientProfileController || {};
var DisplayPatientProfile ="#patientProfile-page";
PatientProfileController = (function () {
var patientProfile;
var presList = [];
var labAppoList = [];
  function init() {
  
  $(DisplayPatientProfile).on("pagebeforeshow", function () {
  DisplayPatientProfilePage();  
	}); 
				 
  } // init()
  
    function setPatientProfile(obj){
  patientProfile=obj;
   $.mobile.changePage("#patientProfile-page"); 
  }
 function DisplayPatientProfilePage(){
  $("#patientInfo").empty();
 var pFName= App.currentPatient.FirstName;
 var pMName=App.currentPatient.MiddleName;
 var pLName=App.currentPatient.LastName;
 
 var pGender= App.currentPatient.Gender;
 var GenderType;
 if (pGender ==1){
 GenderType= "user_male";
 }
 else {
 GenderType= "user_female";
 }
 
 var html="";

     html= $("#patientInfo").append('<table data-role="table" cellspacing="0" class="personalInfo-table" style="width:100%;text-align:center"><tbody><tr><td class="patientInfoCell"><img src="img/'+GenderType+'.png"/></td><td class="patientInfoCell">'
  + pFName+ ' '+ pMName+ ' '+ pLName+ '</td></tr><tr><td class="patientInfoCell" style="color:gray">'+App.currentPatient.PatientID+
  '</td><td class="PersonalInfoIcon"><img src="img/patientprofile/id.png" height="40" width="40"/></td></tr><tr><td class="patientInfoCell" style="color:gray">' +App.currentPatient.Age+
  '</td><td class="PersonalInfoIcon"><img src="img/patientprofile/age.png" height="40" width="40"/></td></tr></tbody></table>'+
  '<table data-role="table" cellspacing="0" style="width:100%;text-align:center"><tbody><tr style="border: thin solid rgb(213, 213, 213)">'+
  '<td class="personalInfoTableCell" style="border-top: 1px solid rgb(213, 213, 213); border-bottom: 1px solid rgb(213, 213, 213);text-align:center"><a href="tel:'+App.currentPatient.MobileNumber+'"><img src="img/patientprofile/phone.png" height="30" width="40"/></a></td>'+
  '<td style="border-top: 1px solid rgb(213, 213, 213); border-bottom: 1px solid rgb(213, 213, 213); text-align:center"><a href="mailto:'+App.currentPatient.EmailAddress+'"><img src="img/patientprofile/email.png" height="24" width="40"/></a></td></tr></tbody></table>');
if(App.currentPatient.PatientStayType==1){
if(App.currentPatient.isDischarged==true){
  $('#iconegGroup2').show();
  var htmlTableRow='<tr><td><input id="getProgressNotes" type="image"  src="img/patientprofile/progressNote.png" data-inline="true" onclick="ProgressNotesController.getProgressNote();"></td><td><input id="getApproval" type="image"  src="img/patientprofile/check.png" data-inline="true" onclick="ApprovalController.getApproval();"></td><td  style="border-right: 0px;border-left: 0px;"><input id="getOrderSheet" type="image"  src="img/patientprofile/order.png" data-inline="true" onclick="ProgressNotesController.getInpatientOrders();"></td></tr>'
  $('#iconegGroup2').empty().append(htmlTableRow);
 }else{
 $('#iconegGroup2').show();
  var htmlTableRow='<tr><td><input id="getProgressNotes" type="image"  src="img/patientprofile/progressNote.png" data-inline="true" onclick="ProgressNotesController.getProgressNote();"></td><td><input id="getApproval" type="image"  src="img/patientprofile/check.png" data-inline="true" onclick="ApprovalController.getApproval();"></td><td><input id="referBtn" type="image"  src="img/patientprofile/Referpatient.png" data-inline="true" onclick="ReferralController.goToReferralPage();"></td><td  style="border-right: 0px;border-left: 0px;"><input id="getOrderSheet" type="image"  src="img/patientprofile/order.png" data-inline="true" onclick="ProgressNotesController.getInpatientOrders();"></td></tr>'
  $('#iconegGroup2').empty().append(htmlTableRow);
 }
 }else{
 var htmlTableRow='<tr><td style="border-right: 0px;border-left: 0px;"><input id="getApproval" type="image"  src="img/patientprofile/check.png" data-inline="true" onclick="ApprovalController.getApproval();"></td></tr>'
 $('#iconegGroup2').empty().append(htmlTableRow);
 }
 }
 
  
 
  // $('#getProgressNotes').on("click",function(){
  // ProgressNotesController.getProgressNote();
  // });
  
    $('#getVitalSigns').on("click",function(){
	  var transNo=0;
	  if(App.currentPatient.PatientStayType==2){
	  transNo=0;
	  	 // App.currentPatient.PatientID= 1212;     //to be removed
	  //App.currentPatient.ProjectID=12; //to be removed
	  }else{
	  transNo=parseInt(App.currentPatient.AdmissionNo);
	  //transNo=2010000140;//to be removed
	 //App.currentPatient.PatientID= 725261;     //to be removed
	  //App.currentPatient.ProjectID=12; //to be removed
	  }
	     var body = {
       //ProjectID: App.currentPatient.ProjectID,
		PatientID:App.currentPatient.PatientID,
		ProjectID: App.user.ProjectID,
		//PatientID:322395,
		PatientTypeID:App.currentPatient.PatientType,
		InOutPatientType:App.currentPatient.PatientStayType,
		TransNo:transNo
		        // ProjectID: 11,
		// PatientID:615141,
		// PatientTypeID:1,
		// TransNo:2008000068
      };//by jaber
	 // body.ProjectID=App.SetProjectIDForRequest();
      var request = new Req.RequestModel("GtVitalSigns", body, retrieveVitalSignsSuccess, retrieveVitalSignsError);
      Req.sendRequest(request);
	});
  function retrieveVitalSignsSuccess(result){
    var list=[];
  list=result.List_DoctorPatientVitalSign;
  VitalSignsController.setVitalSigns(list);
  }
  function retrieveVitalSignsError(result){
    App.showErrorMsg(result.ErrorEndUserMessage);
  }
     $('#getPrescriptions').on("click",function(){
    if (typeof (App.currentPatient) != "undefined" && App.currentPatient != null) {
	if(App.currentPatient.PatientStayType==1){
		var body = {
        PatientID: App.currentPatient.PatientID,
		//PatientID:1083059,
		ProjectID: App.currentPatient.ProjectID,
		//ProjectID:12,
		AdmissionNo: App.currentPatient.AdmissionNo
		//AdmissionNo:2013000044
      };
	      var request = new Req.RequestModel("prescriptionIn", body, retrievePresSuccess, retrievePresError);
	}else{
	  var body = {
        PatientID: App.currentPatient.PatientID,
		//PatientTypeID:App.currentPatient.PatientType
		//ProjectID: 0,
		 SetupID:0
      };
	  if(App.user.ProjectID==2){
		body.ProjectID=App.currentPatient.ProjectID;
		}else{
			body.ProjectID=0;
			}
	  	   var request = new Req.RequestModel("prescriptionOut", body, retrievePresSuccess, retrievePresError);
	}
      Req.sendRequest(request);
    } 
  });

  function retrievePresSuccess(response) {
   presList = [];
  if(App.currentPatient.PatientStayType==1){
  presList = response.List_PrescriptionReportForInPatient;
  }else{
  presList = response.List_PrescriptionReportByPatientID;
  }
    
	  PrescriptionController.setPrescriptionList(presList);
     //$.mobile.changePage("#prescriptionAppo-page");
  }
    function retrievePresError(response) {
    presList = [];
	 App.showErrorMsg(response.ErrorEndUserMessage);
  }
       $('#getLabResult').on("click",function(){
    if (typeof (App.currentPatient) != "undefined" && App.currentPatient != null) {
      var body = {
     PatientID: App.currentPatient.PatientID,
	  //PatientID:471103,
		PatientTypeID:App.currentPatient.PatientType
		//ProjectID:App.user.ProjectID
		//ProjectID:12
      };
	   body.ProjectID=App.SetProjectIDForRequest();
       var request = new Req.RequestModel("labAppo", body, retrieveLabAppoSuccess, retrieveLabAppoError);
      Req.sendRequest(request);
    } 
  });
   function retrieveLabAppoSuccess(response) {
     labAppoList = response.List_GetLabOreders;
	 LabResultsController.setLabAppointmentList(labAppoList);
  }

  function retrieveLabAppoError(response) {
   labAppoList = [];
    App.showErrorMsg(response.ErrorEndUserMessage);
  }
   $('#getRadiology').on("click",function(){
   var body = {
        // PatientID: 1166920,
		// ProjectID:15
		 PatientID: App.currentPatient.PatientID
		//ProjectID:App.user.ProjectID
       };
	  body.ProjectID=App.SetProjectIDForRequest();
       var request = new Req.RequestModel("radiology", body, retrieveRadSuccess, retrieveRadError);
       Req.sendRequest(request);
  });
 function retrieveRadSuccess(response){
 radAppoList=[];
 radAppoList=response.List_GetRadOreders;
  RadiologyController.setRadOrderList(radAppoList);
 }
  function retrieveRadError(response){
    App.showErrorMsg(response.ErrorEndUserMessage);
 }
  return {
    init: init,
setPatientProfile:setPatientProfile
  }
})();