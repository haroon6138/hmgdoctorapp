var HomeController = HomeController || {};
HomeController = (function () {
  var homePageSel = "#home-page";

  function init() {
    $(homePageSel).on("pagebeforeshow", function () {
	$("#doctorPhoto").attr("src",App.user.DoctorImageURL);
	$("#doctorNameLink").text(App.user.DoctorName);
	
	if (App.LANG == "en") {
	$("#DoctorNameArrow").attr("src","img/leftPanelIcons/arrow.png");
	}else{
	$("#DoctorNameArrow").attr("src","img/leftPanelIcons/arrowFlipped.png");
	}
	});
	 $(document).on("pageinit", "#home-page", function () {    
 
        $( document ).on( "swipeleft swiperight", "#home-page", function( e ) {
            // We check if there is no open panel on the page because otherwise
            // a swipe to close the left panel would also open the right panel (and v.v.).
            // We do this by checking the data that the framework stores on the page element (panel: open).
            if ( $.mobile.activePage.jqmData( "panel" ) !== "open" ) {
                // if ( e.type === "swipeleft"  ) 
				// {
                    // $( "#rightCustomPanel" ).panel( "open" );
					
			
                // } 
				//else 
				if ( e.type === "swiperight" ) 
				{
                    $( "#leftCustomPanel" ).panel( "open" );
                }
            }
        });
		});
		
	//Detect current device type and attach tablet or large screen style sheet (desktop, tablet and mobile)
	   if(categorizr() == "tv") {
				//console.log("Device Type Detected: " + categorizr()); 
				$('head').append('<link rel="stylesheet" href="css/tablet_style.css" type="text/css" />');
			}
	   else if(categorizr() == "desktop") {
				console.log("Device Type Detected: " + categorizr()); 
				//$('head').append('<link rel="stylesheet" href="css/tablet_style.css" type="text/css" />'); for testing
			}
	   else if(categorizr() == "tablet") {
				console.log("Device Type Detected: " + categorizr()); 
				$('head').append('<link rel="stylesheet" href="css/tablet_style.css" type="text/css" />');
			}
	   else if(categorizr() == "mobile") {
				console.log("Device Type Detected: " + categorizr()); 
			}
	   else
	   {
	     console.log("Device Type Detected: " + categorizr()); 
	   }
                   
 
		
 
  } // init()
   function scanQr(){
   	 cordova.plugins.barcodeScanner.scan(
      function (result) {
         // alert("We got a barcode\n" +result.text);
               // "Result: " + result.text + "\n" +
                //"Format: " + result.format + "\n" +
               // "Cancelled: " + result.cancelled);
			   var array = result.text.split(',');
			   var type=1;
			   if(array.length>2){
			   if(array[2]=='2')
			   type=2;
			   }
			   MyPatientsController.getScannedPatientID(array[0],array[1],type);
      }, 
      function (error) {
          alert("Scanning failed: " + error);
      }
   );
	}
 
  return {
    init: init,
	scanQr:scanQr
  }
})();