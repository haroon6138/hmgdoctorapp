var SettingsController = SettingsController || {};
SettingsController = (function () {
var settingsPageSel = "#settings-page";

  function init() {
    $(settingsPageSel).on("pagebeforeshow", function () {
      $("#choice-lang-" + App.LANG).attr("checked", "checked").checkboxradio("refresh");
    });
    $("#changeLang").on('change', function (event) {
      var checked = $("input[name*=choice-lang]:checked").val();
       var page = window.location.href;
      if (isMobile.Windows())
        page = page.substring(page.indexOf("/www/"), page.lastIndexOf(".html") + 5);
      if (checked == "ar") {
        page = page.replace("_en", "_ar");
      } else {
        page = page.replace("_ar", "_en");
      }
      if (App.isMobile.any()) {
	
        var settingsPlugin = plugins.SettingsPlugin;
		//var settingsPlugin = new SettingsPlugin();
       if (App.isUserLogged == true) {
          $.jStorage.set("hmg.user.isChangingLang", true);
          $.jStorage.set("hmg.user.session", App.token);
          $.jStorage.set("hmg.user.token", Req.token);
        }
		
        settingsPlugin.changeLang( {
          "lang": checked
        },successHandler, errorHandler);
      }
     window.location = page + "#settings-page";
    });
  } // init()
  function successHandler(result) {
    // $.growl({
      // title: Loc.msg.success,
      // message: Loc.msg.requestPass
    // });
  }

  function errorHandler(error) {
    // $.growl({
      // title: Loc.msg.error,
      // message: Loc.msg.requestFail
    // });
  }
  return {
    init: init
  }
})();