var exec = require('cordova/exec');
var SettingsPlugin = {
	changeLang : function(option, success, fail) {
	
		cordova.exec(success, fail, "SettingsPlugin", "changeLang", [ option ]);
	}
}
// SettingsPlugin
// var SettingsPlugin = new SettingsPlugin();
module.exports = SettingsPlugin;
