var BloodBankController = BloodBankController || {};
BloodBankController = (function () {
var bloodGroup=[];
  function init() {
    
	$("#bloodBank-page").on("pagebeforeshow", function () {
	$("#bloodGroupDiv").empty();
	$('#bloodGroupSelect').val(0).selectmenu("refresh");
	});
  } // init()
  function getBloodBank(){
  	 var body = {
	 ProjectID:0
	 };
  var request = new Req.RequestModel("bloodBank", body,retrieveBloodBankSuccess, retrieveBloodBankError);
  Req.sendRequest(request);
  }
function retrieveBloodBankSuccess(response){
 bloodGroup=[];
bloodGroup=response.BloodGroupList;
 $.mobile.changePage("#bloodBank-page");
}
function retrieveBloodBankError(response){
     App.showErrorMsg(response.ErrorEndUserMessage);
}
$("#bloodGroupSelect").on("change", function (){
fillBloodGroup();
});
  function fillBloodGroup(){
  var html="";

	var list=bloodGroup;
	$("#bloodGroupDiv").empty();
  if(list){
   var str = $("#bloodGroupSelect").val();
   var index = str.indexOf(".");
    var res = str.substring(0,index);
	var bloodType=res; 
	
	
	var str2= $("#bloodGroupSelect").val();
	var res2 = str2.substring(index+1,str.length);
	var RHF=res2;

	
html+='<table>';
html+='<tr><th>ProductAlias</th><th>Product Name</th><th>NoOfUnits</th></tr>'
  for(var i=0;i< list.length;i++){ 
    if (bloodType==list[i].BloodGroup && RHF==list[i].RHFactor) {
	html+='<tr><td>'+list[i].ProductAlias+'</td><td>'+list[i].ProductName+'</td><td>'+list[i].NoOfUnits+'</td></tr>';
   }
  }
  html+='</table>';
  $("#bloodGroupDiv").append(html);
  }
  }
  
  return {
    init: init,
	getBloodBank:getBloodBank
  }
})();