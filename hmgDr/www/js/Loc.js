var Loc = Loc || {};
//

// Arabic Localization Object
var arLoc ={
    msg:{
        "error"     :"خطأ",
        "warning"   :"تنبيه",
		"info" :"رساله",
		"msgAlert" :"رساله",		
		"success"	:"تم بنجاح",
		"successSave"	:"تم الحفظ بنجاح",
        "ok"        :"موافق",
		"cancel"	:"إلغاء",
        "send"      :"أرسال",		
		"update"	:"تحديث",			
		"confirm"	:"تأكيد",
        "networkError":"خطأ بالشبكة",
		"noResults"	:"لا توجد نتائج",
		"loginSuccess"	:"تم تسجيل الدخول",
		"logOutSuccess"	:"تم تسجيل الخروج",
		"reloginMessage"	:"تحتاج إلى إعادة تسجيل الدخول",		
		"requestPass"	:"تم طلبك بنجاح",
		"requestFail"	:"لم يتم تحقيق طلبك",
		"errorAtStartUp"	:"حدث خطأ أثناء البدء.",			
		"mustLogin"	:"تسجيل الدخول؟",
		"generalError"	:"حدث خطأ أثناء معالجة طلبك",
		"netErrorMsg"	: "لايوجد اتصال بالإنترنت أوفشل بتحميل المحتوى.",
		"timeoutError"	:"قد تكون تواجه مشكلة مع اتصال الإنترنت الخاص بك أو هناك تأخر في إكمال طلبك. يرجى المحاولة مرة أخرى في وقت لاحق.",
		"NoDataYet"		:"بيانات المريض ليست متاحة بعد. يرجى التحقق مرة أخرى في وقت لاحق.",
		"NoPosition"	:"لم يتم بعد تحديد موقعك الحالي",
		"noOfTriesLogin":"لقد وصلت إلى الحد الأقصى لعدد المحاولات. يرجى إعادة تسجيل الدخول",
		"updateTitle" : "تحديث متوفر"		
		 },
			precriptionPage: {
		"route":		"طريقة الاستخدام",
		"freqTiming":		"عدد مرات الاستخدام",
		"insCovered":		"مغطى من التأمين",
		"durationDays":		"عدد أيام الاستخدام",
		"doctorRemarks":		"ملاحظات الدكتور",
		"createdBy":"createdBy"
    },
		errorDisplayPage: {
        "header":  "عفوا",
        "title":	"كانت هناك مشكلة مع طلبك الأخير",
        "titleDetails":  "سنقوم بإصلاحها  قريبا. يرجى المحاولة مرة أخرى في وقت لاحق.",
		"debugInfo":  "خطأ في التطبيق",
		"buttonText":  "إغلاق الرسالة"
		},
		vitalSignsPage:{
		"date":"التاريخ: ",
		"time":"الوقت: "
		},
		patientSearchResult:{
		"fileNo":"رقم الملف: " ,
		"AdmissionDate":"تاريخ الدخول: ",
		"StayDays": "عدد أيام الإقامة: ",
		"DischargeDate": "تاريخ المغادرة: ",
		"AppointmentDate": "تاريخ الموعد: ",
		"AppointmentTime": "وقت الموعد: "
		
		},
		drSchPage:{
	"date"	:"التاريخ",
	"day"	:"اليوم",
	"workingHr"	:"ساعات العمل"
	},
	labPage: {
			"invoiceTitle"	:"رقم الفاتورة:",
			"DoctorName"	:"الدكتور:",
			"ClinicName"	:"العيادة:",
			"TestDescription"	:"الوصف",
			"ResultValue"	:"النتيجة",
			"ReferenceRange"	:"يتراوح بين",
			"GeneralResultsTitle"	:"Lab Test General Results",
			"SpecialResultsTitle"	:"Lab Test Special Results",
			"appoTitle"	:"تاريخ الموعد:",				
			"pendingDataOrNoRec"	:"نتائج المختبر ليست متاحة بعد . يرجى التحقق مرة أخرى في وقت لاحق." 				
	},
	searchMedicinePage:{
		"notValidInput"	:"البحث يجب أن يكون باللغة الانجليزية",
		"Description": "الوصف: ",
		"Price": "السعر: "
		
	},
	mapPage:{
			"dur":"المدة: ",
		"dis":"المسافة: "
	},
	drProfilePage:{
		"Gender":  "الجنس:",
		"Clinic":  "العيادة:",
		"Hospital":  "المستشفى:",
		"Speciality":  "التخصص:"
		},
	progNotePage:{
	"cancelled":  "ملغي",
	"progNote":"ملاحظات التحسن",
	"newProg":"الملاحظة",
	"orderSheet":"الطلبات",
	"orderNew":"طلب جديد"
	},
	doctorReplyPage:{
	"date":  "التاريخ",
	"time":"الوقت",
	"file":"رقم الملف",
	"mobile":"رقم الجوال",
	"reply":"رد على الطلب",
	"ques":"الطلب:",
	"replyMsg":"يجب تعبئة الرد قبل الإرسال"
	},
	approvalPage: {
		"approvalNo":		"رقم الموافقة",
		"status":		"الحالة",
		"approvalDate":		"تاريخ الموافقة",
		"submiton":		"تاريخ الإرسال",
		"receipton":		"تاريخ الاستلام",
		"procedureName":		"اسم الإجراء",
		"procedurestatus":		"حالة الإجراء",
		"approvalRequest":		"طلب الموافقة",
		"expiry":		"تاريخ الانتهاء",
		"usage": "عدد غير المستخدمة:",
		"usageStatus": "حالة الاستخدام",
		"total":"مجموع عدد غير المستخدمة:"
    },
	referralPage:{
	"myRefPat":"المرضى المُحالون إلي",
	"myRefrdPat":"المرضى المُحولون مني",
	"disRefPat":"المرضى المُحالين المتعافين",
	"disRefrdPat":"المرضى المُحولون المتعافين",
	"referClinic":"العيادة المُحولة",
	"referDoctor":"الطبيب المُحول",
	"referralClinic":"العيادة المُحول إليها",
	"referralDoctor":"الطبيب المُحول إليه",
	"frequency":"نوع التحويل",
	"priority":"الأولوية",
	"MAXResponseTime":"الوقت الأقصى للرد",
	"clinicRemarks":"ملاحضات وتفاصيل العيادة",
	"referAnswer":"الإجابة و الإقتراحات",
	"file":"رقم الملف",
	"chooseClinic" : "اختر العيادة",
		"chooseDoctor":"اختر الدكتور",
		"errorMsg":"يرجى تعبئة جميع الحقول المطلوبة",
		"verifyButton":"تأكيد",
		"replyButton":"إرسال",
		"fillFieldsMsg":"الرجاء تعبئة جميع الحقول"
	},
	loginPage:{
	"chooseProj":"اختر المستشفى",
	"chooseClinic":"اختر العيادة",
	"validLoginMsg":"يرجى تعبئة جميع الحقول المطلوبة"
	},
	myPatientPage:{
	"admissionDate":"تاريخ الدخول:",
	"chooseClinic":"اختر العيادة"
	},
	PatientProfilePage:{
	"Approval":"موافقات التأمين",
	"ProgressNote":"ملاحظات التحسن",
	"Order":"الطلبات",
	"ReferPatient": "حول مريض"
	}
	
}


//English Localization Object
var enLoc ={
    msg:{
        "error"     :"Error",
        "warning"   :"Warning",
		"info" :"Information",
		"msgAlert" :"Message",			
		"success"	:"Success",
		"successSave"	:"It's saved successfully",
        "ok"        :"OK",
		"cancel"	:"Cancel",
        "send"      :"Send",		
		"update"	:"Update",			
		"confirm"	:"Confirm",
        "networkError":"Network Error",
		"noResults"	:"No Results",
		"loginSuccess"	:"You have logged in",
		"logOutSuccess"	:"You have logged out",
		"reloginMessage"	:"You need to re-login",			
		"requestPass"	:"Your request was successful",
		"requestFail"	:"Your request was unsuccessful",
		"errorAtStartUp"	:"An error Occurred while initiating",			
		"mustLogin"	:"Login to Continue?",
		"cancelReminder":"Cancel Reminder?",
		"generalError"	:"An error occurred while processing your request.",
		"netErrorMsg"	:"No internet connection or failed to load content.",
		"timeoutError"	:"You may be experiencing an issue with your Internet connection or the server is causing long delays when processing your request. Please try again later.",		
		"NoDataYet"		:"Patient data is not available yet.Please check again later.",
		"NoPosition"	:"Your Current Location hasn't been detected yet",
		"noOfTriesLogin":"You reached the maximum number of tries. Please re-login",
		"updateTitle" : "Update Is Available"
    },
	precriptionPage: {
		"route":		"Route",
		"freqTiming":		"Frequency Timing",
		"insCovered":		"Insurance Covered",
		"durationDays":		"Duration Days",
		"doctorRemarks":		"Doctor Remarks",
		"createdBy":"createdBy"
    },
	 errorDisplayPage: {
        "header":  "Oops!",
        "title":	"There was a problem with your last request",
        "titleDetails":  "We'll have it fixed shortly. Please try again later.",
		"debugInfo":  "App Crashed - Debug Info",
		"buttonText":  "Close Message"
		},
		vitalSignsPage:{
		"date":"Date:",
		"time":"Time:"
		},
		patientSearchResult:{
		"fileNo":"File No: " ,
		"AdmissionDate":"Admission Date: ",
		"StayDays": "Number of Days: ",
		"DischargeDate": "Discharge Date: ",
		"AppointmentDate": "Appointment Date: ",
		"AppointmentTime": "Appointment Time: "
		},
		drSchPage:{
	"date"	:"Date",
	"day"	:"Day",
	"workingHr"	:"Working Hours"
	},
	labPage: {
			"invoiceTitle"	:"Invoice No: ",
			"DoctorName"	:"Doctor: ",
			"ClinicName"	:"Clinic: ",
			"TestDescription"	:"Test Description",
			"ResultValue"	:"Result Value",
			"ReferenceRange"	:"Reference Range",
			"GeneralResultsTitle"	:"Lab Test General Results",
			"SpecialResultsTitle"	:"Lab Test Special Results",
			"appoTitle"	:"Appt Date: ",
			"pendingDataOrNoRec"	:"Lab results are not available yet or results have not been processed. Please check again later." 				
	},
	searchMedicinePage:{
		"notValidInput"	:"Search must be in English",
		"Description": "Description: ",
		"Price": "Price: "
	},
		mapPage:{
		"dur":"Duration: ",
		"dis":"Distance: "
	},
	drProfilePage:{
		"Gender":  "Gender: ",
		"Clinic":  "Clinic: ",
		"Hospital":  "Hospital: ",
		"Speciality":  "Speciality: "
	},
	progNotePage:{
	"cancelled":  "Cancelled",
	"progNote":"Progress Note",
	"newProg":"New Progress Note",
	"orderSheet":"Orders",
	"orderNew":"New Order"
	},
	doctorReplyPage:{
	"date":  "Date",
	"time":"Time",
	"file":"File No",
	"mobile":"Mobile No",
	"reply":"Reply",
	"ques":"Request:",
	"replyMsg":"You must fill the response area before sending"
	},
	approvalPage: {
		"approvalNo":		"Approval No:",
		"status":		"Status:",
		"approvalDate":		"Approval Date",
		"submiton":		"Submit on",
		"receipton":		"Receipt on",
		"procedureName":		"Procedure Name",
		"procedurestatus":		"Procedure Status",
		"approvalRequest":		"Approval Request",
		"expiry":		"Expiry Date",
		"usage": "Unused Count:",
		"usageStatus": "Usage Status",
		"total":"Total Unused:"
    },
	referralPage:{
	"myRefPat":"My Referral Patients",
	"myRefrdPat":"My Referred Patients",
	"disRefPat":"Referral Discharged",
	"disRefrdPat":"Referred Discharged",
	"referClinic":"Referring Clinic",
	"referDoctor":"Referring Doctor",
	"referralClinic":"Referral Clinic",
	"referralDoctor":"Referral Doctor",
	"frequency":"Frequency",
	"priority":"Priority",
	"MAXResponseTime":"Max Response Time",
	"clinicRemarks":"Clinic Details and Remarks",
    "referAnswer":"Answer/Suggestion",
	"file":"File No",
	"chooseClinic" : "Choose Clinic",
	"chooseDoctor":"Choose Doctor",
	"errorMsg":"Please fill all the required fields",
	"verifyButton":"verify",
		"replyButton":"reply",
		"fillFieldsMsg":"Please fill all fields"
	},
		loginPage:{
	"chooseProj":"Choose Project",
	"chooseClinic":"Choose Clinic",
	"validLoginMsg":"Please fill all the required fields"
	},
	myPatientPage:{
	"admissionDate":"Admission Date:",
	"chooseClinic":"Choose Clinic"
	},
	PatientProfilePage:{
	"Approval":"Insurance Approvals",
	"ProgressNote":"Progress Notes",
	"Order":"Orders",
	"ReferPatient": "Refer Patient"
	}
	
}