﻿/*
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License. 
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.IO;
using System.Windows.Media.Imaging;
using System.Windows.Resources;
using System.IO.IsolatedStorage;


namespace com.hmg.hmgDr
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            //InitializeComponent();
           // this.CordovaView.Loaded += CordovaView_Loaded;
            InitializeComponent();
            this.CordovaView.DisableBouncyScrolling = true;

            String lang;

            IsolatedStorageSettings.ApplicationSettings.TryGetValue<String>("lang", out lang);

            if (lang != null && lang.Equals("ar"))
                this.CordovaView.StartPageUri = new Uri("/www/index_ar.html", UriKind.Relative);
            else
                this.CordovaView.StartPageUri = new Uri("/www/index_en.html", UriKind.Relative);


            this.CordovaView.Loaded += CordovaView_Loaded;
            
        }

        private void CordovaView_Loaded(object sender, RoutedEventArgs e)
        {
            this.CordovaView.Loaded -= CordovaView_Loaded;
        }
    }
}
