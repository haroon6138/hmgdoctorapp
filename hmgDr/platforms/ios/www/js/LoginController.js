var LoginController = LoginController || {};
LoginController = (function () {
  var loginPageSel = "#login-page";
  var isSilentValidation = false;
  var isSilentLoginFlag = false;
  var silentLoginSuccess = function () {};
  var isCallBackLogin = false;
  var userProjectID;
  var projectCount=0;
  var tempDoctorID=0;
  var loginCallBack = function () {};
  function init() {
    $(loginPageSel).on("pagebeforeshow", function () {
	//set the user logout
  App.isUserLogged = false;
  App.user={};
  Req.token = "";
  $.jStorage.deleteKey('hmg.user.current');
  /**************/
	projectCount=0;
	$('#doctorMemberID').val("");
	$('#doctorPassword').val("");
		$("#loginClinicDiv").hide();
	$("#loginProjectDiv").show();
	 var body = {};
  var request = new Req.RequestModel("projectsList", body,retrieveProjectsListSuccess, retrieveProjectsListError);
  Req.sendRequest(request);
    });
 $("input#doctorMemberID").on( "focus",function(){
  $("#loginClinicDiv").hide();
   $('#select-project').val(0).selectmenu("refresh");
 });
 $("input#doctorPassword").on( "focus",function(){
  $("#loginClinicDiv").hide();
   $('#select-project').val(0).selectmenu("refresh");
 });
  } // init()
 function retrieveProjectsListSuccess(response){
  var len = response.ListProject.length;
  var item;
  App.projectsList = [];
   $("#select-project").empty();
  var html='';
  for (var i = 0; i < len; i++) {
    var item = response.ListProject[i];
    if (item.UsingInDoctorApp) {
	    App.projectsList.push(item);
    }
  }
  html += '<option value="0">' + Loc.loginPage.chooseProj + '</option>';
      for (var i = 0; i < App.projectsList.length; i++) {
        var item = App.projectsList[i];
        html += "<option value=" + item.ID + ">" + item.Name + "</option>";
      }
      $("#select-project").append(html).selectmenu("refresh");
  }
  function retrieveProjectsListError(response){
if(projectCount<3){
projectCount++;
   var body = {};
  var request = new Req.RequestModel("projectsList", body,retrieveProjectsListSuccess, retrieveProjectsListError);
  Req.sendRequest(request);
}else{
 App.showErrorMsg(response.ErrorEndUserMessage);
}
  }
   $('#select-project').on("change",function(){
   var isValid=false;
  isValid= validateLoginField();
  if($('#select-project').val()!=0){
  if(isValid==true){
   var memberID=$('#doctorMemberID').val();
   var memberPassword= $('#doctorPassword').val();
   var projectID=parseInt($('#select-project').val());
   userProjectID=projectID;
     var body = {
        UserID : memberID,
		Password :memberPassword,
		ProjectID : projectID
      };
      var request = new Req.RequestModel("memberLogin", body, memberLoginSuccess, memberLoginError);
      Req.sendRequest(request);
   }else{
   $('#select-project').val(0).selectmenu("refresh");
   App.showErrorMsg(Loc.loginPage.validLoginMsg);
   }
   }else{
   $("#loginClinicDiv").hide();
   }
   });
   function memberLoginError(response){
      $("#loginClinicDiv").hide();
	  $('#select-project').val(0).selectmenu("refresh");
     App.showErrorMsg(response.ErrorEndUserMessage);
  }
  // function memberLoginSuccess(response){
   // $.mobile.changePage("#validation-page"); 
  // }
    function memberLoginSuccess(response) {
	tempDoctorID=response.DoctorID;
    if (response.isSMSSent == true) {
      ValidationController.startValidation(response.LogInTokenID, response.DoctorID, userProjectID, validateLoginSuccess, validateLoginError);
    } else {
      isSilentValidation = true;
	  response.LogInTokenID="0000";
      ValidationController.silentValidation(response.LogInTokenID, response.DoctorID, userProjectID, validateLoginSuccess, validateSilentLoginError);
    }
  }
    function validateLoginSuccess(response) {
    Req.token = response.AuthenticationTokenID;
	if(response.DoctorHaveOneClinic){
    var doctor = response.List_DoctorProfile[0];
    App.setUserSignIn(doctor);
	 $.mobile.changePage("#home-page");
	}else{
	$("#loginClinicDiv").show();
	$("#loginProjectDiv").hide();
	fillClinicList(response.List_DoctorsClinic);
	}
   // PushHandler.registerToken();
    // $.growl({
      // title: Loc.msg.success,
      // message: Loc.msg.loginSuccess
    // });
 //$.mobile.changePage("#home-page"); 
  }
function fillClinicList(list){
  var len = list.length;
  var item;
  var html='';
   $("#select-clinic").empty();
  html += '<option value="0">' + Loc.loginPage.chooseClinic + '</option>';
      for (var i = 0; i < len; i++) {
        var item = list[i];
        html += "<option value=" + item.ClinicID + ">" + item.ClinicName + "</option>";
      }
      $("#select-clinic").append(html).selectmenu("refresh");
}
  function validateLoginError(response) {
    App.showErrorMsg(Loc.msg.noOfTriesLogin);
    window.history.go(-1);
  }
  function validateSilentLoginError(response){
      $("#loginClinicDiv").hide();
	  $('#select-project').val(0).selectmenu("refresh");
  App.showErrorMsg(response.ErrorEndUserMessage);
  }
   $('#submitClinicBtn').on("click",function(){
     isValid= validateLoginClinicField();
  if(isValid==true){
    var body = {
	ProjectID:parseInt($('#select-project').val()),
	ClinicID:parseInt($("#select-clinic").val()),
	doctorID:tempDoctorID,
	IsRegistered:true,
	License:true
	};
  var request = new Req.RequestModel("docProfile", body,retrieveProfileSuccess, retrieveProfileError);
  Req.sendRequest(request);
  }else{
     $('#select-clinic').val(0).selectmenu("refresh");
   App.showErrorMsg(Loc.loginPage.validLoginMsg);
  }
    });
 function retrieveProfileSuccess(response){
 var doctor = response.DoctorProfileList[0];
    App.setUserSignIn(doctor);
 $.mobile.changePage("#home-page");
 }
 function retrieveProfileError(response){
     // App.setUserSignIn([]);
 // $.mobile.changePage("#home-page");
 }
   function validateLoginField(){
   if($('#doctorMemberID').val() && $('#doctorPassword').val()&& $('#select-project').val()!=0)
   return true;
   else
   return false;
   }
   function  validateLoginClinicField(){
      var doctorID=$('#doctorMemberID').val();
   var clinicID=$('#select-clinic').val();
   var password=$('#doctorPassword').val();
   var  project=$('#select-project').val();
   if( doctorID &&  password && project!=0){
   if(clinicID!=0)
   return true;
   else
    return false;
   }else{
     $('#select-project').val(0).selectmenu("refresh");
   	$("#loginClinicDiv").hide();
   return false;
   }
   }
    function openLink(url) {
    //var sociallink = url.match(/facebook|twitter|linkedin|youtube/g);
   // GATrackEvent("Button", "Click", sociallink[0] + " Side Button", 1);
    window.open(url, '_system');
  }
  function chnageLanguage(){
      var checked = $("#changeLangBtn").html();
       var page = window.location.href;
      if (isMobile.Windows())
        page = page.substring(page.indexOf("/www/"), page.lastIndexOf(".html") + 5);
      if (checked == "عربي") {
      checked='ar';
        page = page.replace("_en", "_ar");
      } else {
      checked='en';
        page = page.replace("_ar", "_en");
      }
      if (App.isMobile.any()) {
	
        var settingsPlugin = plugins.SettingsPlugin;
		//var settingsPlugin = new SettingsPlugin();
       if (App.isUserLogged == true) {
          $.jStorage.set("hmg.user.isChangingLang", true);
          $.jStorage.set("hmg.user.session", App.token);
          $.jStorage.set("hmg.user.token", Req.token);
        }
		
        settingsPlugin.changeLang( {
          "lang": checked
        },successHandler, errorHandler);
      }
     window.location = page + "#settings-page";
  }
  function successHandler(result) { }

  function errorHandler(error) {}
  return {
    init: init,
	openLink:openLink,
	chnageLanguage:chnageLanguage
  }
})();